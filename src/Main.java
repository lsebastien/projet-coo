import java.awt.BorderLayout;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.swing.JFrame;



import presentation.fenetre.FenetrePrincipale;
import metier.*;
import data.Client;
import data.Forfait;
import data.Reservation;
import data.Salle;
import data.TypeForfait; 
import data.TypeSalle;
import exceptions.ClientExistant;
import exceptions.ClientInexistant;
import exceptions.ForfaitExpirerException;
import exceptions.ForfaitIncompatibleException;
import exceptions.PasDeForfaitsException;
import exceptions.ReservationInexistante;
import exceptions.SalleExistante;
import exceptions.TypeForfaitInexistant;
import exceptions.TypeSalleInexistant;
import fabrique.FabClient;
import fabrique.FabForfait;
import fabrique.FabReservation;
import fabrique.FabSalle;
import fabrique.FabTypeForfait;
import fabrique.FabTypeSalle;


public class Main {

        /**
         * @param args
         * @throws ForfaitExpirerException 
         * @throws ForfaitIncompatibleException 
         * @throws PasDeForfaitsException 
         * @throws ClientExistant 
         * @throws SQLException 
         * @throws ClassNotFoundException 
         * @throws InterruptedException 
         * @throws ClientInexistant 
         * @throws TypeSalleInexistant 
         * @throws SalleExistante 
         * @throws ReservationInexistante 
         * @throws TypeForfaitInexistant 
         */
        public static void main(String[] args) throws PasDeForfaitsException, ForfaitIncompatibleException, ForfaitExpirerException, ClassNotFoundException, SQLException, ClientExistant, InterruptedException, ClientInexistant, TypeSalleInexistant, SalleExistante, ReservationInexistante, TypeForfaitInexistant {
                
                Metier metier = new Metier();
                
                //CREATION DES TYPES DE SALLE
                TypeSalle ts1 = FabTypeSalle.getInstance().rechercherTypeSalle("Petite");
                TypeSalle ts2 = FabTypeSalle.getInstance().rechercherTypeSalle("Moyenne");

                TypeSalle ts3 = FabTypeSalle.getInstance().rechercherTypeSalle("Grande");

                
                //CREATION DES SALLES
              //  Salle s1 = FabSalle.getInstance().rechercherSalle("Petite1");
               // Salle s2 = FabSalle.getInstance().rechercherSalle("Petite2");

              //  Salle s3 = FabSalle.getInstance().rechercherSalle("Moyenne1");
        
                //CREATION DES CLIENTS
              //  Client c1 = FabClient.getInstance().rechercherClient("Thomas",1254);
               // Client c2 = FabClient.getInstance().rechercherClient("Jerome",852147);
                
                //Client c2 = FabClient.getInstance().creerClient("Jean",123456);
                //Client c3 = FabClient.getInstance().creerClient("Marcel",654321);
                
                for (Iterator<Client> iter = FabClient.getInstance()
                                .listerClient(); iter.hasNext();) {
                        Client c = iter.next();
                        System.out.println(c.getNomClient());
                }
                // CALCUL DES DATES DE PEREMPTION DES FORFAITS
              //  Calendar limiteForfaitTroismois = Calendar.getInstance();
              //  limiteForfaitTroismois.add(Calendar.MONTH,3);
               // Calendar limiteForfaitSixmois = Calendar.getInstance();
               // limiteForfaitSixmois.add(Calendar.MONTH,6);
                
                // TYPES DE FORFAIT EN FONCTION DE LA DUREE ET DU TYPE DE SALLE
               // TypeForfait forfait_12h_petite = FabTypeForfait.getInstance().rechercherTypeForfait(1);
               // TypeForfait forfait_24h_petite = FabTypeForfait.getInstance().rechercherTypeForfait(2);
               // TypeForfait forfait_12h_grande = FabTypeForfait.getInstance().rechercherTypeForfait(3);
               // TypeForfait forfait_24h_grande = FabTypeForfait.getInstance().rechercherTypeForfait(4);
               
              //  Forfait forfait_c1 = FabForfait.getInstance().creerForfait(forfait_12h_petite.getCreditTemps(), limiteForfaitTroismois, c1, forfait_12h_petite, ts1);
               // Calendar cal = Calendar.getInstance();
               // cal.set(cal.get(Calendar.YEAR), Calendar.NOVEMBER, 25);
                
                //CREATION DES RESERVATIONS
               // Reservation r0 = FabReservation.getInstance().creerReservation(cal,1,"Matin",9,Calendar.getInstance(),metier.PrixReservation(s1,2), c1, s1);
                //Reservation r1 = FabReservation.getInstance().creerReservation(cal,1,"Matin",10,Calendar.getInstance(),metier.PrixReservation(s1,2), c1, s1);

                //Reservation r2 = FabReservation.getInstance().creerReservation(Calendar.getInstance(),2,"Matin",8,Calendar.getInstance(),metier.PrixReservation(s2,2),c2, s2);
                
              //  System.out.println("RESERVATION 1------------------------------------------------------------");
              //  System.out.println("PRIX A PAYER :"+metier.PayerReservation(r0.getId(),c1.getIdClient(),forfait_c1,false));
              //  System.out.println("RESERVATION 2------------------------------------------------------------");
        //        System.out.println("PRIX A PAYER :"+metier.PayerReservation(r2.getId(),c1.getIdClient(), false,false));
             //   System.out.println("RESERVATION 3------------------------------------------------------------");
              //  System.out.println("PRIX A PAYER :"+metier.PayerReservation(r1.getId(),c1.getIdClient(),forfait_c1,true));
              //  System.out.println("Points de fidelite du client C1: "+c1.getPfidelite());
                
                // BONUS
              //  ArrayList<Reservation> lesReservationSallePetite = (ArrayList<Reservation>) metier.RechercheReservationTypeSalle("Petite");
                
              //  HashMap<String,ArrayList<Reservation>> lePlanning = new HashMap<String,ArrayList<Reservation>>();
              //  lePlanning.put("matin", new ArrayList<Reservation>());
              //  lePlanning.put("midi", new ArrayList<Reservation>());
              // lePlanning.put("soir", new ArrayList<Reservation>());
                
                //Test UseCase2
              //  UseCaseReservationAutomatique us = new UseCaseReservationAutomatique();
        /*
                System.out.println(cal.getTime());
                if(us.isJourTravaille(cal)){
                        System.out.println(us.effectuerReservation(cal, "Petite","Matin", 1,"Thomas",1254));
                        System.out.println(us.effectuerReservation(cal, "Petite","Matin", 1,"Thomas",1254));
                        System.out.println(us.effectuerReservation(cal, "Petite","Midi", 7,"Thomas",1254));
                        System.out.println(us.effectuerReservation(cal, "Petite","Midi", 1,"Thomas",1254));
                        System.out.println(us.effectuerReservation(cal, "Petite","Soir", 1,"Thomas",1254));
                        System.out.println(us.effectuerReservation(cal, "Petite","Soir", 1,"Thomas",1254));
                        System.out.println(us.effectuerReservation(cal, "Petite","Soir", 2,"Thomas",1254));
                        System.out.println(us.effectuerReservation(cal, "Petite","Soir", 1,"Thomas",1254));
                        
                        for(Reservation r : c1.getMesReservations()){
                                System.out.println(r.getStatut());
                                
                        }
                        
                        System.out.println(us.effectuerReservation(cal, "Moyenne","Matin", 1,"Thomas",1254));
                        System.out.println(us.effectuerReservation(cal, "Moyenne","Matin", 1,"Thomas",1254));
                        System.out.println(us.effectuerReservation(cal, "Moyenne","Midi", 7,"Thomas",1254));
                        System.out.println(us.effectuerReservation(cal, "Moyenne","Midi", 1,"Thomas",1254));
                        System.out.println(us.effectuerReservation(cal, "Moyenne","Soir", 1,"Thomas",1254));
                        System.out.println(us.effectuerReservation(cal, "Moyenne","Soir", 1,"Thomas",1254));
                        System.out.println(us.effectuerReservation(cal, "Moyenne","Soir", 2,"Thomas",1254));
                        System.out.println(us.effectuerReservation(cal, "Moyenne","Soir", 1,"Thomas",1254));
                }
                else
                        System.out.println("Je travaille pas");
                */
                /*
                System.out.println("==================================");
                System.out.println("Affichage planning");
                System.out.println("==================================");
                
                UseCaseAfficherPlanning us2 = new UseCaseAfficherPlanning();
                Map<Salle, List<Reservation>> map = new HashMap<Salle, List<Reservation>>();
                map = us2.getInfoPlanning("Moyenne", cal);
                
                for(Entry<Salle, List<Reservation>> entry : map.entrySet()) {
                    Salle s = entry.getKey();
                    List<Reservation> maListe = entry.getValue();
                    System.out.println(s.getNomSalle());
                    for (Reservation r : maListe){
                            System.out.println(r.getHoraire());
                    }
                }*/
                
                //Cr�ation Fen�tre principale
                JFrame frame = new JFrame("Application");
                frame.getContentPane().add(new FenetrePrincipale(frame,metier),BorderLayout.CENTER);
                frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
                frame.setSize(800,400);
                frame.setLocationRelativeTo(null);
                frame.setVisible(true);
                
        }
        

}