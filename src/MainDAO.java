import java.io.FileNotFoundException;
import java.io.FileReader;
import java.lang.reflect.InvocationTargetException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;

import bdd.maConnection;
import data.Client;
import exceptions.ClientExistant;
import exceptions.ClientInexistant;
import fabrique.FabClient;


public class MainDAO {

	public static void main(String[] args) throws ClassNotFoundException, ClientInexistant, SQLException, ClientExistant {
		
		Client c1 = FabClient.getInstance().rechercherClient("Bobby",1254);
		//Client c2 = FabClient.getInstance().rechercherClient("Maxime",46);
		//Client c3 = FabClient.getInstance().creerClient("NASA",10214);
		for (Iterator<Client> iter = FabClient.getInstance()
				.listerClient(); iter.hasNext();) {
			Client c = iter.next();
			System.out.println(c.getNomClient());
		}
		System.out.println("------------------------------------------------------");
		//c1.setNomClient("TESTUPDATE");
		
		for (Iterator<Client> iter = FabClient.getInstance()
				.listerClient(); iter.hasNext();) {
			Client c = iter.next();
			System.out.println(c.getNomClient());
		}
		
		System.out.println(c1.getClass());
		  try {
			    List<Object> mesValues = new ArrayList<Object>();
			  	maConnection con = new maConnection();
				FileReader reader = new FileReader("client.json");
				JSONTokener tokener = new JSONTokener(reader);
				JSONObject root = new JSONObject(tokener);
				Iterator<?> keys = root.keys();
				String SQL = "UPDATE CLIENT SET ";
				while( keys.hasNext() ){
					String key = (String)keys.next();
			        java.lang.reflect.Method method;
			        method = Client.class.getMethod((String)root.get(key));
			        mesValues.add(method.invoke(c1));
			        SQL += key+" = ?";
			        if(keys.hasNext()){
						SQL += " , ";
					}
			    }
				SQL += " WHERE ID = 3 ";
				System.out.println(SQL);
				PreparedStatement preparedStatement = con.getConnection().prepareStatement(SQL);
				for(Object o : mesValues){
					preparedStatement.setObject(mesValues.indexOf(o)+1,o);
				}
			
				System.out.println(preparedStatement.executeUpdate());
			} catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (SecurityException e) {
				  // ...
				} catch (NoSuchMethodException e) {
				  // ...
				
				} catch (IllegalAccessException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (IllegalArgumentException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (InvocationTargetException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
		org.hsqldb.DatabaseManager.closeDatabases(0);
	}
	

}
