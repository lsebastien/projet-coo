package bdd;


import java.io.FileNotFoundException;
import java.io.FileReader;
import java.lang.reflect.InvocationTargetException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;

import data.Client;

public class maConnection {
	
	private Connection con;
	
	public maConnection() throws ClassNotFoundException, SQLException{
		Class.forName("org.hsqldb.jdbcDriver");
		con = DriverManager.getConnection("jdbc:hsqldb:file:bdd/Reservation;shutdown=true;hsqldb.write_delay=false;","admin","admin");
	}
	
	public boolean Update(Object obj){
		try {
			
		    List<Object> mesValues = new ArrayList<Object>();
		  	Connection con = this.con;
		  	
		
		  
			FileReader reader = new FileReader(obj.getClass().getName()+".json");
			JSONTokener tokener = new JSONTokener(reader);
			JSONObject root = new JSONObject(tokener);
			Iterator<?> keys = root.keys();
			String SQL = "UPDATE CLIENT SET ";
			Object id = "";
			String finsql = "";
		   while( keys.hasNext() ){
					String key = (String)keys.next();
			        java.lang.reflect.Method method;
			        method = obj.getClass().getMethod((String)root.get(key));
			        if(key.equals("ID")){
			        	 finsql = " WHERE ID = ?";
			        	 id= method.invoke(obj);
			        	 
			        }else{
				        mesValues.add(method.invoke(obj));
				        SQL += key+" = ?";
				        if(keys.hasNext()){
							SQL += " , ";
						}
			        }
			    }
		       
		        
		        
			SQL += finsql;
			System.out.println(SQL);
			System.out.println(mesValues);
			PreparedStatement preparedStatement = con.prepareStatement(SQL);
			for(Object o : mesValues){
				preparedStatement.setObject(mesValues.indexOf(o)+1,o);
			}
			preparedStatement.setObject(mesValues.size()+1,id);
		
			int bupdate = preparedStatement.executeUpdate();
			con.commit();
			
			if(bupdate == 1){
				System.out.println("YES");
				return true;
			}else{
				return false;
			}
			} catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (SecurityException e) {
			  // ...
			} catch (IllegalAccessException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IllegalArgumentException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (InvocationTargetException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (NoSuchMethodException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		org.hsqldb.DatabaseManager.closeDatabases(0);
		return false;
	}
	
	public Connection getConnection(){
		return this.con;
	}
	
	

}