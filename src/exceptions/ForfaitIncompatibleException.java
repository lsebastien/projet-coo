package exceptions;

public class ForfaitIncompatibleException extends Exception{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public ForfaitIncompatibleException() {}

   //Constructor that accepts a message
   public ForfaitIncompatibleException(String message)
   {
      super(message);
   }
}
