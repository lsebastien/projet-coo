package exceptions;

public class TypeSalleInexistant extends Exception {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
   //Constructor that accepts a message
   public TypeSalleInexistant(String message)
   {
      super(message);
   }
}
