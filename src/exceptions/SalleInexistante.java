package exceptions;

public class SalleInexistante extends Exception {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
   //Constructor that accepts a message
   public SalleInexistante(String message)
   {
      super(message);
   }
}
