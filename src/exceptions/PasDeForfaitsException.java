package exceptions;

public class PasDeForfaitsException extends Exception{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public PasDeForfaitsException() {}

   //Constructor that accepts a message
   public PasDeForfaitsException(String message)
   {
      super(message);
   }
}
