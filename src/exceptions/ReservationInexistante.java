package exceptions;

public class ReservationInexistante extends Exception {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;



   //Constructor that accepts a message
   public ReservationInexistante(String message)
   {
      super(message);
   }
}