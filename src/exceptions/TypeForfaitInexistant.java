package exceptions;

public class TypeForfaitInexistant extends Exception{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public TypeForfaitInexistant() {}

   //Constructor that accepts a message
   public TypeForfaitInexistant(String message)
   {
      super(message);
   }
}
