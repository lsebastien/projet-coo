package exceptions;

public class ForfaitExpirerException extends Exception {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public ForfaitExpirerException() {}

   //Constructor that accepts a message
   public ForfaitExpirerException(String message)
   {
      super(message);
   }
}
