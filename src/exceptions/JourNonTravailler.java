package exceptions;

public class JourNonTravailler extends Exception {
	
	   /**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public JourNonTravailler(String message)
	   {
	      super(message);
	   }

}
