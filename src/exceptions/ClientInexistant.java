package exceptions;

public class ClientInexistant extends Exception {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;



   //Constructor that accepts a message
   public ClientInexistant(String message)
   {
      super(message);
   }
}
