package exceptions;

public class ClientExistant extends Exception {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
   //Constructor that accepts a message
   public ClientExistant(String message)
   {
      super(message);
   }
}
