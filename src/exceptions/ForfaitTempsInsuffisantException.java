package exceptions;

public class ForfaitTempsInsuffisantException extends Exception {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public ForfaitTempsInsuffisantException() {}

   //Constructor that accepts a message
   public ForfaitTempsInsuffisantException(String message)
   {
      super(message);
   }
}
