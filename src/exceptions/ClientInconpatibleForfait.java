package exceptions;

public class ClientInconpatibleForfait extends Exception{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public ClientInconpatibleForfait() {}

   //Constructor that accepts a message
   public ClientInconpatibleForfait(String message)
   {
      super(message);
   }
}