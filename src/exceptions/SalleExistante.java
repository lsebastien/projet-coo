package exceptions;

public class SalleExistante extends Exception {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
   //Constructor that accepts a message
   public SalleExistante(String message)
   {
      super(message);
   }
}

