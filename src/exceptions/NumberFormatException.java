package exceptions;

public class NumberFormatException extends Exception{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public NumberFormatException() {}

   //Constructor that accepts a message
   public NumberFormatException(String message)
   {
      super(message);
   }
}
