package fabrique;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import bdd.maConnection;
import data.Client;
import data.Forfait;
import data.Reservation;
import data.TypeForfait;
import data.TypeSalle;
import exceptions.ClientExistant;
import exceptions.ClientInexistant;
import exceptions.PasDeForfaitsException;
import exceptions.TypeForfaitInexistant;
import exceptions.TypeSalleInexistant;

public class FabForfait {
	
	private static FabForfait singleton;
	private ArrayList<Forfait> tsLesForfaits;
	
	private maConnection con;	
	public static FabForfait getInstance(){
		if (singleton==null) singleton = new FabForfait();
		return singleton;
	}
		
	private FabForfait(){
		this.tsLesForfaits = new ArrayList<Forfait>();
	}
	
	public Forfait creerForfait(int heureRestantes, Calendar dateExpiration,Client monClient, TypeForfait monTypeForfait, TypeSalle monTypeSalle) throws SQLException, ClassNotFoundException {
		
		con = new maConnection();
		int dernier_forfait_inserer = 0;
		

		PreparedStatement pInsertForfait = con
				.getConnection()
				.prepareStatement(
						"INSERT INTO FORFAIT (HEURE_RESTANTES,DATE_EXPIRATION,CLIENT_ID,TYPE_FORFAIT,TYPE_SALLE) VALUES(?,?,?,?,?)",
						java.sql.Statement.RETURN_GENERATED_KEYS);
		pInsertForfait.setInt(1, heureRestantes);
		pInsertForfait.setLong(2,dateExpiration.getTimeInMillis());
		pInsertForfait.setInt(3,monClient.getId());
		pInsertForfait.setInt(4,monTypeForfait.getIdTypeForfait());
		pInsertForfait.setInt(5,monTypeSalle.getIdType());
		
		pInsertForfait.executeUpdate();
		ResultSet KeysForfait = pInsertForfait.getGeneratedKeys();
		if (KeysForfait.next()) {

			dernier_forfait_inserer = KeysForfait.getInt(1);

		}

		Forfait f = new Forfait(dernier_forfait_inserer,heureRestantes,dateExpiration,monClient,monTypeForfait,monTypeSalle);
		tsLesForfaits.add(f);
		monClient.addUnForfait(f);
		
		con.getConnection().close();
		this.tsLesForfaits.add(f);
		
		return f;
	}
	
	public Forfait rechercherForfait(int idForfait) throws SQLException, PasDeForfaitsException, ClassNotFoundException, ClientInexistant, TypeSalleInexistant, TypeForfaitInexistant {
		con = new maConnection();
	
		PreparedStatement pStatementForfait = con.getConnection().prepareStatement("SELECT * from FORFAIT where id = ?");
		pStatementForfait.clearParameters();
		pStatementForfait.setInt(1,idForfait);
		ResultSet rsForfait = pStatementForfait.executeQuery();
		if(!rsForfait.next()){
			throw new PasDeForfaitsException("le Forfait "+ idForfait+" n'existe pas ");	
		}
		Calendar date_expiration = Calendar.getInstance();
		date_expiration.setTimeInMillis(rsForfait.getLong("DATE_EXPIRATION"));
		Forfait f = new Forfait(rsForfait.getInt("ID"),rsForfait.getInt("HEURE_RESTANTES"),date_expiration,FabClient.getInstance().rechercherClientById(rsForfait.getInt("CLIENT_ID")), FabTypeForfait.getInstance().rechercherTypeForfait(rsForfait.getInt("TYPE_FORFAIT")), FabTypeSalle.getInstance().rechercherTypeSalleById(rsForfait.getInt("TYPE_SALLE")));
		return f;
	}
	
	public ArrayList<Forfait> rechercherForfaitByIdClient(int idClient){
		ArrayList<Forfait> lesForfaits = new ArrayList<Forfait>();
		
			try {
				con = new maConnection();
				PreparedStatement pStatementForfait = con.getConnection().prepareStatement("SELECT * from FORFAIT where CLIENT_ID = ?");
				pStatementForfait.clearParameters();
				pStatementForfait.setInt(1,idClient);
				ResultSet rsForfait = pStatementForfait.executeQuery();
				
				while(rsForfait.next()){
					Calendar date_expiration = Calendar.getInstance();
					date_expiration.setTimeInMillis(rsForfait.getLong("DATE_EXPIRATION"));
					Forfait f = new Forfait(rsForfait.getInt("ID"), rsForfait.getInt("HEURE_RESTANTES"),date_expiration, FabClient.getInstance().rechercherClientById(rsForfait.getInt("CLIENT_ID")), FabTypeForfait.getInstance().rechercherTypeForfait(rsForfait.getInt("TYPE_FORFAIT")), FabTypeSalle.getInstance().rechercherTypeSalleById(rsForfait.getInt("TYPE_SALLE")));
					lesForfaits.add(f);
				}
			} catch (ClassNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (ClientInexistant e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (TypeForfaitInexistant e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (TypeSalleInexistant e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			
			
		return lesForfaits;
		
	}
	
	public void supprimerForfait(int idForfait) {
		tsLesForfaits.remove(idForfait);
	}
	
	
	
	public Iterator<Forfait> listerForfaits() throws ClassNotFoundException, SQLException, ClientInexistant, TypeSalleInexistant, TypeForfaitInexistant {
		con = new maConnection();
		Statement stmt = con.getConnection().createStatement();
		ResultSet rsForfait = stmt.executeQuery("SELECT * FROM FORFAIT");
		while (rsForfait.next()) {
			Calendar date_expiration = Calendar.getInstance();
			date_expiration.setTimeInMillis(rsForfait.getLong("DATE_EXPIRATION"));
			tsLesForfaits.add(new Forfait(rsForfait.getInt("ID"),rsForfait.getInt("HEURE_RESTANTE"),date_expiration,FabClient.getInstance().rechercherClientById(rsForfait.getInt("CLIENT_ID")), FabTypeForfait.getInstance().rechercherTypeForfait(rsForfait.getInt("TYPE_FORFAIT")), FabTypeSalle.getInstance().rechercherTypeSalleById(rsForfait.getInt("TYPE_SALLE"))));

		}

		con.getConnection().close();
		return tsLesForfaits.iterator();
	}

}
