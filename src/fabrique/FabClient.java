package fabrique;

import java.sql.Statement;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import bdd.maConnection;
import data.Client;
import exceptions.ClientExistant;
import exceptions.ClientInexistant;
import exceptions.ReservationInexistante;

public class FabClient {

	private static FabClient singleton;
	private List<Client> tsLesClients;
	private int idClient = 0;
	public static FabClient getInstance() {
		if (singleton == null)
			singleton = new FabClient();
		return singleton;
	}

	private maConnection con;

	private FabClient() {
		this.tsLesClients = new ArrayList<Client>();
	}

	public Client creerClient(String nomClient, int telClient)
			throws ClassNotFoundException, SQLException, ClientExistant {
		con = new maConnection();
		int dernier_client_inserer = 0;
		PreparedStatement pStatementClient = con.getConnection()
				.prepareStatement(
						"SELECT * from CLIENT where nom = ? and telephone = ?");
		pStatementClient.clearParameters();
		pStatementClient.setString(1, nomClient);
		pStatementClient.setInt(2, telClient);
		ResultSet rsClient = pStatementClient.executeQuery();
		if (rsClient.next()) {
			throw new ClientExistant("Le client " + nomClient
					+ " éxiste deja !");
		}

		PreparedStatement pInsertPersonne = con
				.getConnection()
				.prepareStatement(
						"INSERT INTO CLIENT (NOM,TELEPHONE,POINTS_FIDELITE) VALUES(?,?,0)",
						java.sql.Statement.RETURN_GENERATED_KEYS);
		pInsertPersonne.setString(1, nomClient);
		pInsertPersonne.setInt(2, telClient);
		pInsertPersonne.executeUpdate();
		ResultSet KeysClient = pInsertPersonne.getGeneratedKeys();
		if (KeysClient.next()) {

			dernier_client_inserer = KeysClient.getInt(1);

		}

		Client c = new Client(dernier_client_inserer, nomClient, telClient);
		tsLesClients.add(c);
		con.getConnection().close();
		return c;
	}

	public Client rechercherClient(String nomClient, int telephoneClient)
			throws ClientInexistant, SQLException, ClassNotFoundException {
		con = new maConnection();
		Client c;
		PreparedStatement pStatementClient = con
				.getConnection()
				.prepareStatement(
						"SELECT ID,NOM,TELEPHONE,POINTS_FIDELITE from CLIENT where NOM = ? and TELEPHONE = ?");
		pStatementClient.clearParameters();
		pStatementClient.setString(1, nomClient);
		pStatementClient.setInt(2, telephoneClient);
		ResultSet rsPersonne = pStatementClient.executeQuery();
		while (rsPersonne.next()) {
			if (rsPersonne.getString("NOM").equals(nomClient) && rsPersonne.getInt("TELEPHONE") == telephoneClient) {
				c = new Client(rsPersonne.getInt("ID"),rsPersonne.getString("NOM"),	rsPersonne.getInt("TELEPHONE"));
				c.setPfidelite(rsPersonne.getInt("POINTS_FIDELITE"));
				tsLesClients.add(c);
				return c;
			}
		}
		con.getConnection().commit();
		con.getConnection().close();
		throw new ClientInexistant("La client n'existe pas !");

	}
	
	public Client rechercherClientById(int idClient)
			throws ClientInexistant, SQLException, ClassNotFoundException {
		con = new maConnection();
		Client c;
		PreparedStatement pStatementClient = con
				.getConnection()
				.prepareStatement(
						"SELECT ID,NOM,TELEPHONE,POINTS_FIDELITE from CLIENT where ID = ?");
		pStatementClient.clearParameters();
		pStatementClient.setInt(1, idClient);
	
		ResultSet rsPersonne = pStatementClient.executeQuery();
		while (rsPersonne.next()) {
			if (rsPersonne.getInt("ID") == idClient) {
				c = new Client(rsPersonne.getInt("ID"),
						rsPersonne.getString("NOM"),
						rsPersonne.getInt("TELEPHONE"));
				c.setPfidelite(rsPersonne.getInt("POINTS_FIDELITE"));
				tsLesClients.add(c);
				return c;
			}
		}
		con.getConnection().close();
		throw new ClientInexistant("La client n'existe pas !");

	}
	
	public Client rechercherClientByIdReservation(int idReservation)
			throws ClientInexistant, SQLException, ClassNotFoundException {
		con = new maConnection();
		Client c;
		PreparedStatement pStatementClient = con
				.getConnection()
				.prepareStatement(
						"SELECT ID,NOM,TELEPHONE,POINTS_FIDELITE from CLIENT,RESERVATION WHERE RESERVATION.CLIENT_ID = CLIENT.ID AND RESERVATION.ID = ?");
		pStatementClient.clearParameters();
		pStatementClient.setInt(1, idReservation);
		System.out.println(idReservation);
		ResultSet rsPersonne = pStatementClient.executeQuery();
		if(!rsPersonne.next()){
			throw new ClientInexistant("La client n'existe pas !");
		}
		
				c = new Client(rsPersonne.getInt("ID"),
						rsPersonne.getString("NOM"),
						rsPersonne.getInt("TELEPHONE"));
				c.setPfidelite(rsPersonne.getInt("POINTS_FIDELITE"));
		
				con.getConnection().close();
				return c;
	}
		
		
		

	
	
	public Client rechercherClientExist(String nomClient, int numTel) {
		for (Client c : tsLesClients) {
			if (c.getNomClient().equals(nomClient)
					&& c.getTelClient() == numTel) {
				return c;
			}
		}

		return null;

	}

	public void supprimerClient(int idCLient) {
		tsLesClients.remove(idClient);
	}
	public Iterator<Client> listerClient() throws SQLException,
			ClassNotFoundException {
		tsLesClients.clear();
		con = new maConnection();
		Statement stmt = con.getConnection().createStatement();
		ResultSet rsClient = stmt.executeQuery("SELECT * FROM CLIENT");
		while (rsClient.next()) {
			tsLesClients.add(new Client(rsClient.getInt("ID"), rsClient
					.getString("NOM"), rsClient.getInt("TELEPHONE")));
		}

		con.getConnection().close();
		return tsLesClients.iterator();
	}
	
	public String updateNomClient(Client c,String nom) throws SQLException, ClassNotFoundException{

		con = new maConnection();
		PreparedStatement pStatementClient = con.getConnection().prepareStatement("UPDATE CLIENT SET NOM = ? WHERE ID = ?");
		pStatementClient.clearParameters();
		pStatementClient.setString(1,nom);
		pStatementClient.setInt(2, c.getId());
	
		System.out.println(pStatementClient.executeUpdate());
		con.getConnection().close();
		return nom;
		
	}

}
