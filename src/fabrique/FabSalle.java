package fabrique;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import bdd.maConnection;
import data.Client;
import data.Salle;
import data.TypeSalle;
import exceptions.ClientExistant;
import exceptions.ClientInexistant;
import exceptions.SalleExistante;
import exceptions.TypeSalleInexistant;

public class FabSalle {
	private static FabSalle singleton;
	private List<Salle> ttsLesSalles;
	private int idSalle = 0;
	
	public static FabSalle getInstance(){
		if (singleton==null) singleton = new FabSalle();
		return singleton;
	}
	private maConnection con;	
	private FabSalle(){
		this.ttsLesSalles = new ArrayList<Salle>();
	}
	
	public Salle creerSalle(String nomSalle, TypeSalle monTypeSalle) throws SQLException, ClassNotFoundException, SalleExistante {
		con = new maConnection();
		int derniere_salle_insereee = 0;
		PreparedStatement pStatementSalle = con.getConnection()
				.prepareStatement(
						"SELECT * from SALLE where nom_salle = ? and type_salle = ?");
		pStatementSalle.clearParameters();
		pStatementSalle.setString(1, nomSalle);
		pStatementSalle.setInt(2, monTypeSalle.getIdType());
		ResultSet rsSalle = pStatementSalle.executeQuery();
		if (rsSalle.next()) {
			throw new SalleExistante("La salle " + nomSalle
					+ " éxiste deja !");
		}
		PreparedStatement pInsertSalle = con
				.getConnection()
				.prepareStatement(
						"INSERT INTO SALLE (NOM_SALLE,TYPE_SALLE) VALUES(?,?)",
						java.sql.Statement.RETURN_GENERATED_KEYS);
		pInsertSalle.setString(1, nomSalle);
		pInsertSalle.setInt(2, monTypeSalle.getIdType());
		pInsertSalle.executeUpdate();
		ResultSet KeysSalle = pInsertSalle.getGeneratedKeys();
		if (KeysSalle.next()){
			derniere_salle_insereee = KeysSalle.getInt(1);
		}
		Salle s = new Salle(derniere_salle_insereee,nomSalle,monTypeSalle);
		this.ttsLesSalles.add(s);
		con.getConnection().close();
		return s;
	}
	
	public Salle rechercherSalle(String nom) throws SQLException, ClassNotFoundException, ClientInexistant, TypeSalleInexistant {
		con = new maConnection();
		Salle s;
		PreparedStatement pStatementSalle = con
				.getConnection()
				.prepareStatement(
						"SELECT ID,NOM_SALLE,TYPE_SALLE from SALLE where NOM_SALLE = ?");
		pStatementSalle.clearParameters();
		
		pStatementSalle.setString(1, nom);
		ResultSet rsSalle = pStatementSalle.executeQuery();
		while (rsSalle.next()) {
				s = new Salle(rsSalle.getInt("ID"),rsSalle.getString("NOM_SALLE"),FabTypeSalle.getInstance().rechercherTypeSalleById(rsSalle.getInt("TYPE_SALLE")));
				return s;
		}
		con.getConnection().close();
		throw new ClientInexistant("La salle n'existe pas !");

	}
	
	public Salle rechercherSalleById(int idSalle) throws SQLException, ClassNotFoundException, ClientInexistant, TypeSalleInexistant {
		con = new maConnection();
		Salle s;
		PreparedStatement pStatementSalle = con
				.getConnection()
				.prepareStatement(
						"SELECT ID,NOM_SALLE,TYPE_SALLE from SALLE where ID = ?");
		pStatementSalle.clearParameters();
		
		pStatementSalle.setInt(1,idSalle);
		ResultSet rsSalle = pStatementSalle.executeQuery();
		while (rsSalle.next()) {
				s = new Salle(rsSalle.getInt("ID"),rsSalle.getString("NOM_SALLE"),FabTypeSalle.getInstance().rechercherTypeSalleById(rsSalle.getInt("TYPE_SALLE")));
				return s;
		}
		con.getConnection().close();
		throw new ClientInexistant("La salle n'existe pas !");

	}
	
	public void supprimerSalle(int idSalle) {
		ttsLesSalles.remove(idSalle);
	}
	
	public List<Salle> rechercherSalleByTypeSalle(int typeSalle) {
		List<Salle> mesSalles = new ArrayList<Salle>();
		try {
			con = new maConnection();
			PreparedStatement pStatementSalle = con
					.getConnection()
					.prepareStatement(
							"SELECT ID,NOM_SALLE,TYPE_SALLE from SALLE where TYPE_SALLE = ?");
			pStatementSalle.clearParameters();
			
			pStatementSalle.setInt(1, typeSalle);
			ResultSet rsSalle = pStatementSalle.executeQuery();
			while (rsSalle.next()) {
				
					mesSalles.add(new Salle(rsSalle.getInt("ID"),rsSalle.getString("NOM_SALLE"),FabTypeSalle.getInstance().rechercherTypeSalleById(rsSalle.getInt("TYPE_SALLE"))));
					
				
			}
			con.getConnection().commit();
			con.getConnection().close();
		} catch (ClassNotFoundException | SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (TypeSalleInexistant e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return mesSalles;
	}
	
	public Iterator<Salle> listerSalle()  {
		ttsLesSalles.clear();
		
			try {
				con = new maConnection();
				Statement stmt = con.getConnection().createStatement();
				ResultSet rsSalles = stmt.executeQuery("SELECT * FROM SALLE");
				while (rsSalles.next()) {
					ttsLesSalles.add(new Salle(rsSalles.getInt("ID"),rsSalles.getString("NOM_SALLE"),FabTypeSalle.getInstance().rechercherTypeSalleById(rsSalles.getInt("TYPE_SALLE"))));
				}

				con.getConnection().close();
			} catch (ClassNotFoundException | SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (TypeSalleInexistant e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		return ttsLesSalles.iterator();
	}
	
}
