package fabrique;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import bdd.maConnection;
import data.Client;
import data.Forfait;
import data.Reservation;
import data.Salle;
import exceptions.ClientInexistant;
import exceptions.PasDeForfaitsException;
import exceptions.ReservationInexistante;
import exceptions.TypeSalleInexistant;

public class FabReservation {
	private static FabReservation singleton;
	private List<Reservation> ttsLesReservations;
	private int idReservation = 0;
	private maConnection con;	
	public static FabReservation getInstance(){
		if (singleton==null) singleton = new FabReservation();
		return singleton;
	}
		
	private FabReservation(){
		this.ttsLesReservations = new ArrayList<Reservation>();
	}
	
	public Reservation creerReservation(Calendar date, int duree,	String horaire, int heureDebut, Calendar dateReservation, int prix,
		Client monClient, Salle maSalle) throws SQLException, ClassNotFoundException{
		int derniere_reservation_inserted = 0;
		con = new maConnection();
		PreparedStatement pInsertReservation = con
				.getConnection()
				.prepareStatement(
						"INSERT INTO RESERVATION (DATE,DUREE,HEURE_DEBUT,HORAIRE,STATUT,DATE_RESERVATION,DATE_CONFIRMATION,PRIX,CLIENT_ID,SALLE) VALUES(?,?,?,?,?,?,?,?,?,?)",
						java.sql.Statement.RETURN_GENERATED_KEYS);
		pInsertReservation.setLong(1,date.getTimeInMillis());
		pInsertReservation.setInt(2,duree);
		pInsertReservation.setInt(3,heureDebut);
		pInsertReservation.setString(4,horaire);
		pInsertReservation.setString(5,"nc");
		pInsertReservation.setLong(6,dateReservation.getTimeInMillis());
		pInsertReservation.setLong(7,0);
		pInsertReservation.setInt(8,0);
		pInsertReservation.setInt(9,monClient.getId());
		pInsertReservation.setInt(10,maSalle.getIdSalle());
		
		pInsertReservation.executeUpdate();
		ResultSet KeysReservation = pInsertReservation.getGeneratedKeys();
		if (KeysReservation.next()) {
			derniere_reservation_inserted = KeysReservation.getInt(1);
		}
		Reservation r = new Reservation(derniere_reservation_inserted, date, duree, horaire, heureDebut, dateReservation, prix, monClient, maSalle);
		ttsLesReservations.add(r);
		con.getConnection().commit();
		con.getConnection().close();
		maSalle.addUneReservation(r);
		monClient.addUneReservation(r);
		return r;
	}
	
	public Reservation rechercherReservation(int idReservation) throws SQLException, ClassNotFoundException, ReservationInexistante, ClientInexistant, TypeSalleInexistant {
		con = new maConnection();
		
		PreparedStatement pStatementReservation = con.getConnection().prepareStatement("SELECT * from RESERVATION where id = ?");
		pStatementReservation.clearParameters();
		pStatementReservation.setInt(1,idReservation);
		ResultSet rsReservation = pStatementReservation.executeQuery();
		if(!rsReservation.next()){
			throw new ReservationInexistante("la reservation "+ idReservation+ " n'existe pas ");	
		}
		Calendar date = Calendar.getInstance();
		date.setTimeInMillis(rsReservation.getLong("DATE"));
		Calendar date_reservation = Calendar.getInstance();
		date_reservation.setTimeInMillis(rsReservation.getLong("DATE_RESERVATION"));
		Reservation r = new Reservation(idReservation, date, rsReservation.getInt("DUREE"),rsReservation.getString("HORAIRE"), rsReservation.getInt("HEURE_DEBUT"), date_reservation, rsReservation.getInt("PRIX"), FabClient.getInstance().rechercherClientById(rsReservation.getInt("client_id")), FabSalle.getInstance().rechercherSalleById(rsReservation.getInt("SALLE")));
		r.setStatut(rsReservation.getString("STATUT"));
		return r;
	}
	
	public void supprimerReservation(int idReservation) {
		ttsLesReservations.remove(idReservation);
	}
	
	public ArrayList<Reservation> getReservationByIdSalle(int idSalle) {
		ArrayList<Reservation> mesReservationsParSalle = new ArrayList<Reservation>();  
		try {
			con = new maConnection();
			PreparedStatement pStatementReservation = con.getConnection().prepareStatement("SELECT * from RESERVATION where SALLE = ?");
			pStatementReservation.clearParameters();
			pStatementReservation.setInt(1,idSalle);
			ResultSet rsReservation = pStatementReservation.executeQuery();
			
			while(rsReservation.next()){
				Calendar date = Calendar.getInstance();
				date.setTimeInMillis(rsReservation.getLong("DATE"));
				Calendar date_reservation = Calendar.getInstance();
				date_reservation.setTimeInMillis(rsReservation.getLong("DATE_RESERVATION"));
				Reservation r = new Reservation(idReservation, date, rsReservation.getInt("DUREE"),rsReservation.getString("HORAIRE"), rsReservation.getInt("HEURE_DEBUT"), date_reservation, rsReservation.getInt("PRIX"), FabClient.getInstance().rechercherClientById(rsReservation.getInt("client_id")), FabSalle.getInstance().rechercherSalleById(rsReservation.getInt("SALLE")));
				r.setStatut(rsReservation.getString("STATUT"));
				mesReservationsParSalle.add(r);
			}
			
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ClientInexistant e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (TypeSalleInexistant e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
			
			
			return mesReservationsParSalle;
		}
	
	
	public ArrayList<Reservation> getReservationsByIdClient(int idClient){
		ArrayList<Reservation> mesReservationsParClient = new ArrayList<Reservation>();  
		try {
			con = new maConnection();
			PreparedStatement pStatementReservation = con.getConnection().prepareStatement("SELECT * from RESERVATION where CLIENT_ID = ?");
			pStatementReservation.clearParameters();
			pStatementReservation.setInt(1,idClient);
			ResultSet rsReservation = pStatementReservation.executeQuery();
			
			while(rsReservation.next()){
				Calendar date = Calendar.getInstance();
				date.setTimeInMillis(rsReservation.getLong("DATE"));
				Calendar date_reservation = Calendar.getInstance();
				date_reservation.setTimeInMillis(rsReservation.getLong("DATE_RESERVATION"));
				Reservation r = new Reservation(rsReservation.getInt("ID"), date, rsReservation.getInt("DUREE"),rsReservation.getString("HORAIRE"), rsReservation.getInt("HEURE_DEBUT"), date_reservation, rsReservation.getInt("PRIX"), FabClient.getInstance().rechercherClientById(rsReservation.getInt("client_id")), FabSalle.getInstance().rechercherSalleById(rsReservation.getInt("SALLE")));
				r.setStatut(rsReservation.getString("STATUT"));
				mesReservationsParClient.add(r);
			}
			
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ClientInexistant e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (TypeSalleInexistant e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
			
		return mesReservationsParClient;
		
	}
	
	public Iterator<Reservation> listerReservation(){
		
		ttsLesReservations.clear();
		try {
			con = new maConnection();
			Statement stmt = con.getConnection().createStatement();
			ResultSet rsReservation = stmt.executeQuery("SELECT * FROM RESERVATION");
			while (rsReservation.next()){
				Calendar date = Calendar.getInstance();
				date.setTimeInMillis(rsReservation.getLong("DATE"));
				Calendar date_reservation = Calendar.getInstance();
				date_reservation.setTimeInMillis(rsReservation.getLong("DATE_RESERVATION"));
				ttsLesReservations.add(new Reservation(idReservation, date, rsReservation.getInt("DUREE"),rsReservation.getString("HORAIRE"), rsReservation.getInt("HEURE_DEBUT"), date_reservation, rsReservation.getInt("PRIX"), FabClient.getInstance().rechercherClientById(rsReservation.getInt("client_id")), FabSalle.getInstance().rechercherSalleById(rsReservation.getInt("SALLE"))));
			}
			con.getConnection().close();
		} catch (ClassNotFoundException | SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ClientInexistant e) {
			
			e.printStackTrace();
		} catch (TypeSalleInexistant e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		return ttsLesReservations.iterator();
	}

}
