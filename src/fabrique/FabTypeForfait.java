package fabrique;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import bdd.maConnection;
import data.Client;
import data.Forfait;
import data.TypeForfait;
import exceptions.PasDeForfaitsException;
import exceptions.TypeForfaitInexistant;

public class FabTypeForfait {
	
	private static FabTypeForfait singleton;
	private ArrayList<TypeForfait> tsLesTypesForfait;
	private int idTypeForfait = 0;
	private maConnection con;	
	public static FabTypeForfait getInstance(){
		if (singleton==null) singleton = new FabTypeForfait();
		return singleton;
	}
		
	private FabTypeForfait(){
		this.tsLesTypesForfait = new ArrayList<TypeForfait>();
	}
	
	public TypeForfait creerTypeForfait(int creditTemps, int dureeValidite,int prix)  {
		TypeForfait t = new TypeForfait(this.idTypeForfait,creditTemps,dureeValidite,prix);
		this.tsLesTypesForfait.add(t);
		this.idTypeForfait++;
		return t;
	}
	
	public TypeForfait rechercherTypeForfait(int idTypeForfait) throws ClassNotFoundException, SQLException, TypeForfaitInexistant{
		
		con = new maConnection();
		
		PreparedStatement pStatementForfait = con.getConnection().prepareStatement("SELECT * from TYPEFORFAIT where id = ?");
		pStatementForfait.clearParameters();
		pStatementForfait.setInt(1,idTypeForfait);
		ResultSet rsForfait = pStatementForfait.executeQuery();
		if(!rsForfait.next()){
			throw new TypeForfaitInexistant("le TypeForfait "+ idTypeForfait +" n'existe pas ");	
		}
		
		TypeForfait tf = new TypeForfait(rsForfait.getInt("ID"), rsForfait.getInt("CREDIT_TEMPS"), rsForfait.getInt("TEMPS_VALIDITE_MOIS"), rsForfait.getInt("PRIX"));
		con.getConnection().close();
		return tf;
	}
	
	public void supprimerTypeForfait(int idTypeForfait) {
		tsLesTypesForfait.remove(idTypeForfait);
	}
	
	public Iterator<TypeForfait> listerTypeForfait() {
		return tsLesTypesForfait.iterator();
	}
	

}
