package fabrique;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.lang.reflect.InvocationTargetException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;

import com.sun.org.apache.xml.internal.serializer.ToUnknownStream;

import bdd.maConnection;

public class FabSQL {

	private static FabSQL singleton;

	public static FabSQL getInstance() throws SQLException, ClassNotFoundException{
		if (singleton==null) singleton = new FabSQL();
		return singleton;
	}
	private maConnection con;
		
	private FabSQL() {
		
		
	}
	
	public boolean Update(Object obj){
		try {
			
		    List<Object> mesValues = new ArrayList<Object>();
		    con = new maConnection();
		  	
		
		  
			FileReader reader = new FileReader(obj.getClass().getName()+".json");
			
			JSONTokener tokener = new JSONTokener(reader);
			JSONObject root = new JSONObject(tokener);
			Iterator<?> keys = root.keys();
			String SQL = "UPDATE "+ obj.getClass().getName().substring(5).toUpperCase()+" SET ";
			Object id = "";
			String finsql = "";
		   while( keys.hasNext() ){
					String key = (String)keys.next();
			        java.lang.reflect.Method method;
			        method = obj.getClass().getMethod((String)root.get(key));
			        if(key.equals("ID")){
			        	 finsql = " WHERE ID = ?";
			        	 id= method.invoke(obj);
			        	 
			        }else{
				        mesValues.add(method.invoke(obj));
				        SQL += key+" = ?";
				        if(keys.hasNext()){
							SQL += " , ";
						}
			        }
			    }
		       
		        
		        
			SQL += finsql;
			System.out.println(SQL);
			System.out.println(mesValues);
			PreparedStatement preparedStatement = con.getConnection().prepareStatement(SQL);
			for(Object o : mesValues){
				preparedStatement.setObject(mesValues.indexOf(o)+1,o);
			}
			preparedStatement.setObject(mesValues.size()+1,id);
			System.out.println(id);
		
			int bupdate = preparedStatement.executeUpdate();
			con.getConnection().commit();
			con.getConnection().close();
			
			if(bupdate == 1){
				System.out.println("YES");
				return true;
			}else{
				return false;
			}
			} catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (SecurityException e) {
			  // ...
			} catch (IllegalAccessException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IllegalArgumentException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (InvocationTargetException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (NoSuchMethodException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (ClassNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		return false;
	}
	
	public boolean Delete(Object obj){
		
		System.out.println(obj.getClass().getName().substring(5).toUpperCase());
		if(obj.getClass().getName().substring(5).toUpperCase().equals("RESERVATION")){
			
			try {
				 con = new maConnection();
				PreparedStatement preparedStatement = con.getConnection().prepareStatement("DELETE FROM RESERVATION WHERE ID = ?");
				java.lang.reflect.Method method;
				method = obj.getClass().getMethod("getId");
				preparedStatement.setObject(1,method.invoke(obj));
				System.out.println(method.invoke(obj));
				preparedStatement.executeUpdate();
				con.getConnection().commit();
				con.getConnection().close();
			} catch (NoSuchMethodException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (SecurityException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IllegalAccessException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IllegalArgumentException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (InvocationTargetException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (ClassNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
		}
		
		return false;
	}

}
