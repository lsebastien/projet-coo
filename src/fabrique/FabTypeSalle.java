package fabrique;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import bdd.maConnection;
import data.Client;
import data.TypeSalle;
import exceptions.ClientInexistant;
import exceptions.TypeSalleInexistant;

public class FabTypeSalle {
	private static FabTypeSalle singleton;
	private ArrayList<TypeSalle> tsLesTypesSalle;
	private int idTypeSalle = 0;
	
	public static FabTypeSalle getInstance(){
		if (singleton==null) singleton = new FabTypeSalle();
		return singleton;
	}
	private maConnection con;	
	private FabTypeSalle(){
		this.tsLesTypesSalle = new ArrayList<TypeSalle>();
	}
	
	public TypeSalle creerTypeSalle(String nomType){
		TypeSalle ts = new TypeSalle(this.idTypeSalle,nomType);
		this.tsLesTypesSalle.add(ts);
		this.idTypeSalle++;
		return ts;
	}
	
	public TypeSalle rechercherTypeSalle(String TypeSalle) throws TypeSalleInexistant {
		try {
			con = new maConnection();
			
			PreparedStatement pStatementTypeSalle = con
					.getConnection()
					.prepareStatement(
							"SELECT ID,NOM_TYPE from TYPESALLE where NOM_TYPE = ?");
			pStatementTypeSalle.clearParameters();
		
			pStatementTypeSalle.setString(1, TypeSalle);
			ResultSet rsTypeSalle = pStatementTypeSalle.executeQuery();
			if (rsTypeSalle.next()) {
				
				TypeSalle ts = new TypeSalle(rsTypeSalle.getInt("ID"),
							rsTypeSalle.getString("NOM_TYPE"));
					return ts;
				
			}
			con.getConnection().close();
		} catch (ClassNotFoundException | SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		throw new TypeSalleInexistant("Le type de salle n'existe pas");
		


	}
	
	public TypeSalle rechercherTypeSalleById(int idTypeSalle) throws TypeSalleInexistant{
		try {
			con = new maConnection();
			PreparedStatement pStatementTypeSalle = con
					.getConnection()
					.prepareStatement(
							"SELECT ID,NOM_TYPE from TYPESALLE where id = ?");
			pStatementTypeSalle.clearParameters();
		
			pStatementTypeSalle.setInt(1, idTypeSalle);
			ResultSet rsTypeSalle = pStatementTypeSalle.executeQuery();
			if (rsTypeSalle.next()) {
				
				TypeSalle ts = new TypeSalle(rsTypeSalle.getInt("ID"),
							rsTypeSalle.getString("NOM_TYPE"));
					return ts;
				
			}
			con.getConnection().close();
		} catch (ClassNotFoundException | SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		throw new TypeSalleInexistant("Le type de salle n'existe pas");
		


	}
	
	public void supprimerTypeSalle(int idTypeSalle) {
		tsLesTypesSalle.remove(idTypeSalle);
	}
	
	public Iterator<TypeSalle> listerTypeSalle() {
		tsLesTypesSalle.clear();
		try {
			con = new maConnection();
			Statement stmt = con.getConnection().createStatement();
			ResultSet rsTypeSalle = stmt.executeQuery("SELECT * FROM TYPESALLE ");
			while (rsTypeSalle.next()) {
				tsLesTypesSalle.add(new TypeSalle(rsTypeSalle.getInt("ID"),rsTypeSalle.getString("NOM_TYPE")) );
			}

			con.getConnection().close();
			
		} catch (ClassNotFoundException | SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return tsLesTypesSalle.iterator();
		
	}
	

}
