package presentation.model;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

import javax.swing.DefaultListModel;

import data.TypeSalle;
import fabrique.FabReservation;
import fabrique.FabTypeSalle;

public class TypeSalleModel extends DefaultListModel {

	public TypeSalleModel() {
		for (Iterator iter = FabTypeSalle.getInstance().listerTypeSalle(); iter
				.hasNext();) {
			Object o = iter.next();
			addElement(((TypeSalle)o).getNomType());
		}
		 
	}
	
}
