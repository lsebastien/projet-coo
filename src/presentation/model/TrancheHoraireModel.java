package presentation.model;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.swing.DefaultListModel;

import data.TypeSalle;
import fabrique.FabTypeSalle;

public class TrancheHoraireModel extends DefaultListModel{
	
	public TrancheHoraireModel() {
		List<String> maListeTH = new ArrayList<String>();
		maListeTH.add("Matin");
		maListeTH.add("Midi");
		maListeTH.add("Soir");
		for (String s : maListeTH) {
			addElement(s);
		}
		 
	}

}
