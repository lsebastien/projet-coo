package presentation.model;

import java.awt.Color;
import java.awt.Component;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPopupMenu;
import javax.swing.ListCellRenderer;

import data.Reservation;

public class ListReservationRenderer extends JLabel implements ListCellRenderer {

	public Component getListCellRendererComponent(JList list, Object value,
			int index, boolean isSelected, boolean cellHasFocus) {
		Reservation r = (Reservation) value;
		SimpleDateFormat format_francais = new SimpleDateFormat("dd/MM/yyyy");
		if (isSelected) {
			setBackground(new Color(0, 205, 0));
			setForeground(Color.white);
		} else {
			setBackground(list.getBackground());
			setForeground(list.getForeground());
		}
		setFont(list.getFont());
		setOpaque(true);

		
			if (r.getStatut().equals("conf")) {
				setForeground(Color.blue);
			} else if (r.getStatut().equals("nc")) {
				setForeground(Color.orange);
			} else {
				setForeground(Color.red);
			}
			setEnabled(true);
			setText(r.getId() + " - "
					+ format_francais.format(r.getDate().getTime()) + " - "
					+ r.getMaSalle().getMonTypeSalle().getNomType() + " - "
					+ r.getStatut());
		
		

		return this;
	}

}
