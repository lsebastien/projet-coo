package presentation.model;

import java.text.SimpleDateFormat;

import javax.swing.DefaultListModel;

import data.Client;
import data.Forfait;
import data.Reservation;
import fabrique.FabForfait;
import fabrique.FabReservation;

public class ForfaitsModel extends DefaultListModel {

	public ForfaitsModel(Client c) {
		for (Forfait f : FabForfait.getInstance().rechercherForfaitByIdClient(c.getId())) {
			System.out.println("aa");
			SimpleDateFormat format_francais = new SimpleDateFormat("dd/MM/yyyy");

			addElement(f.getId()+" - "+f.getHeureRestantes()+" - "+format_francais.format(f.getDateExpiration().getTime())+"-"+f.getMonTypeSalle().getNomType());
		}
		 
	}
	
}

