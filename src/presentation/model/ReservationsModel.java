package presentation.model;

import java.awt.Color;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Iterator;

import javax.swing.DefaultListModel;

import data.Client;
import data.Reservation;
import data.TypeSalle;
import fabrique.FabReservation;
import fabrique.FabTypeSalle;

public class ReservationsModel<Reservation> extends DefaultListModel {

	public ReservationsModel(Client c) {
		for (data.Reservation r : FabReservation.getInstance().getReservationsByIdClient(c.getId())){
			if (r.getDate().compareTo(Calendar.getInstance()) > 0) {
						addElement(r);
			}
		}
		 
	}
	
}
