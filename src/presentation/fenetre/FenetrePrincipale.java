package presentation.fenetre;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Calendar;
import java.util.Iterator;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

import data.Reservation;
import fabrique.FabReservation;
import metier.Metier;
import metier.UseCaseEditerInfoClient;
import metier.UseCaseReservationAutomatique;
import metier.UseCaseReservationMulti;

public class FenetrePrincipale extends JPanel implements ActionListener {

	private JFrame frame;
	private Metier metier;
	private JLabel labelTitre = new JLabel("Mon application");

	private JButton boutonUseCase1 = new JButton("Visualiser les Reservations");
	private JButton boutonUseCase2 = new JButton("Reservations");
	private JButton boutonUseCase4 = new JButton("Edition clients / Annulation");
	private JButton boutonUseCase5 = new JButton(
			"Effectuer reservations frequentes");

	private JFrame frameUseCase1;

	@Override
	public void actionPerformed(ActionEvent e) {
		Object o = e.getSource();
		if (o == boutonUseCase1) {
			useCase1();
		} else if (o == boutonUseCase2) {
			useCase2();
		}

		else if (o == boutonUseCase4) {
			useCase4();
		}

		else if (o == boutonUseCase5) {
			useCaseResaMulti();
		}

		else
			return;

	}
	
	private void useCase2() {
		UseCaseReservationAutomatique monUseCase2 = new UseCaseReservationAutomatique();
		JFrame fenetre = new JFrame();
		fenetre.getContentPane().add(new FenetreUseCase2(fenetre,monUseCase2),BorderLayout.CENTER);
		fenetre.setTitle("Effectuer Reservation");
		fenetre.setSize(800,400);
		fenetre.setLocationRelativeTo(null);
		fenetre.setVisible(true);
	}

	private void useCase1() {
		//Mis à jour etats reservations
		Iterator<Reservation> i = FabReservation.getInstance().listerReservation();
		
		while (i.hasNext()){
			Reservation r = (Reservation) i.next();
			if (r.getDateReservation().compareTo(Calendar.getInstance())<0 && !(r.getStatut().equals("hdc)"))){
				r.setStatut("hdc");
				System.out.println(r.getDate());
				System.out.println(r.getStatut());
			}

		}
		
		JFrame fenetre = new JFrame();
		fenetre.getContentPane().add(new FenetreUseCase1(fenetre,metier),BorderLayout.CENTER);
		fenetre.setTitle("Visualisation Planning");
		fenetre.setSize(800,400);
		fenetre.setLocationRelativeTo(null);
		fenetre.setVisible(true);
	}
	
	private void useCaseResaMulti() {
		UseCaseReservationMulti metierUseCaseMulti = new UseCaseReservationMulti();
		JFrame fenetre = new JFrame();
		fenetre.getContentPane().add(new FenetreUseCaseResaMulti(fenetre,metierUseCaseMulti),BorderLayout.CENTER);
		fenetre.setTitle("Effectuer Reservation Mutiple");
		fenetre.setSize(800,400);
		fenetre.setLocationRelativeTo(null);
		fenetre.setVisible(true);
	}

private void useCase4() {
		UseCaseEditerInfoClient monUseCase4 = new UseCaseEditerInfoClient();
		JFrame fenetre = new JFrame();
		fenetre.getContentPane().add(new FenetreUseCase4(fenetre,monUseCase4),BorderLayout.CENTER);
		fenetre.setTitle("Visualisation Planning");
		fenetre.setSize(800,400);
		fenetre.setLocationRelativeTo(null);
		fenetre.setVisible(true);
	}

	public FenetrePrincipale(JFrame Frame, Metier metier) {
		super(true);
		this.frame = frame;
		this.metier = metier;

		boutonUseCase1.addActionListener(this);
		boutonUseCase2.addActionListener(this);
		boutonUseCase4.addActionListener(this);
		boutonUseCase5.addActionListener(this);

		JPanel boutonPanel = new JPanel();
		boutonPanel.add(boutonUseCase1);
		boutonPanel.add(boutonUseCase2);
		boutonPanel.add(boutonUseCase4);
		boutonPanel.add(boutonUseCase5);

		setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));

		labelTitre.setAlignmentX(Component.CENTER_ALIGNMENT);
		add(labelTitre);
		add(boutonPanel);
	}

}
