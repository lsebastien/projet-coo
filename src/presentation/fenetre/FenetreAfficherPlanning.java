package presentation.fenetre;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.ListCellRenderer;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import presentation.MonListCellRender;
import presentation.MonTableCellRenderer;
import presentation.model.MyTableModel;
import data.Reservation;
import data.Salle;
import exceptions.JourNonTravailler;
import exceptions.TypeSalleInexistant;
import metier.Metier;
import metier.UseCaseAfficherPlanning;

public class FenetreAfficherPlanning extends JPanel {

	private JFrame frame;
	private String typeSalle = "";
	private Metier metier;
	private Calendar maDate;
	
	public FenetreAfficherPlanning(JFrame Frame, String typeSalle, Calendar maDate) throws TypeSalleInexistant, JourNonTravailler {
		this.frame = frame;
		this.typeSalle = typeSalle;
		this.maDate = maDate;

		UseCaseAfficherPlanning us = new UseCaseAfficherPlanning();
		Map<String, List<Reservation>> map = us.getInfoPlanning(typeSalle, maDate);
		
		Set cles = map.keySet();
		//(String[]) cles.toArray(new String[cles.size()]);
		
		Object[][] tableData = new Object[15][map.keySet().size()+1];
		
		
		int index_2 = 0;
		int index = 0;
		for(int i = 9 ; i<24 ; i++){
			tableData[index][index_2] = Integer.toString(i);
			index++;
		}
		index_2++;
		
		
		for (String key : map.keySet())
		{
			int horaire=9;
			index = 0;
			List<Reservation> valeur = map.get(key);
			for (Reservation r : valeur){
				int duree = 0;
				while(horaire<24){
					if(r.getHeureDebut()==horaire){
						while (duree<r.getDuree()){
							tableData[index][index_2] = r.getStatut();
							index++;
							duree++;
							horaire++;
						}
						break;
					}
					else {
						tableData[index][index_2] = "libre";
						index++;
						horaire++;
					}
				}
				
				
			}
		    while(index<15){
		    	tableData[index][index_2] = "libre";
		    	index++;
		    }
		    index_2++;
		}
		
		String[] titreheure = {"Heure"};
		String[] list = (String[]) cles.toArray(new String[cles.size()]);
		String[] titre ;
		titre = new String[titreheure.length + list.length];
        System.arraycopy(titreheure, 0, titre, 0, titreheure.length);
        System.arraycopy(list, 0, titre, titreheure.length, list.length);
		JTable tableau = new JTable();
		MyTableModel model = new MyTableModel(tableData, titre);
		tableau.setModel(model);
		for (int i=1 ; i< titre.length; i++){
		tableau.getColumnModel().getColumn(i).setCellRenderer(new MonTableCellRenderer());
		}
	    add(new JScrollPane(tableau));

	}

}
