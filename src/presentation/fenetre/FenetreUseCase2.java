package presentation.fenetre;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.Calendar;

import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.ListCellRenderer;
import javax.swing.border.Border;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import presentation.MonListCellRender;
import presentation.model.TrancheHoraireModel;
import presentation.model.TypeSalleModel;

import com.toedter.calendar.JCalendar;

import exceptions.JourNonTravailler;
import exceptions.TypeSalleInexistant;
import exceptions.trancheHoraireInexistante;
import metier.Metier;
import metier.UseCaseReservationAutomatique;

public class FenetreUseCase2 extends JPanel implements ActionListener,
		ListSelectionListener {

	private JFrame frame;
	private JDialog dialog;
	private JLabel labelTypeSalle = new JLabel("Votre Type de Salle :");
	private String typeSalle = "";
	private JLabel labelTrancheHoraire = new JLabel("Votre tranche horaire");
	private String trancheHoraire = "";
	private JLabel labelDuree = new JLabel("Votre duree");
	private int duree;
	private Calendar maDate;
	JCalendar calendar;
	private JLabel labelClient = new JLabel("Votre nom");
	private String nomClient = "";
	private JLabel labelTel = new JLabel("Votre t�l�phone");
	private int numTel;
	private JButton boutonPossible = new JButton("Verifier cr�naux dispo");
	private JButton boutonReservation = new JButton("Valider cette r�servation");
	private JLabel labelDispo = new JLabel("Le crenau dispo est : ");

	private JFrame frameUseCase2;
	private JList liste;
	private JList liste2;

	private UseCaseReservationAutomatique metier;
	private JTextField dureeTX;
	private JTextField clientTX;
	private JTextField telTX;

	public FenetreUseCase2(JFrame Frame,
			UseCaseReservationAutomatique monUseCase2) {
		super(true);
		this.frame = frame;
		this.metier = monUseCase2;

		// Bouton
		boutonPossible.addActionListener(this);
		boutonReservation.addActionListener(this);
		boutonReservation.setVisible(false);
		JPanel boutonPanel = new JPanel();
		boutonPanel.add(boutonPossible);
		boutonPanel.add(boutonReservation);

		// Liste Type Salle
		JPanel typeSallePanel = new JPanel();
		typeSallePanel.setLayout(new BoxLayout(typeSallePanel,
				BoxLayout.LINE_AXIS));
		liste = new JList();
		TypeSalleModel monModel = new TypeSalleModel();
		liste.setModel(monModel);
		liste.addListSelectionListener(this);
		liste.setSize(getMaximumSize());
		ListCellRenderer renderer = new MonListCellRender();
		liste.setCellRenderer(renderer);
		JScrollPane pane = new JScrollPane(liste);
		typeSallePanel.add(labelTypeSalle);
		typeSallePanel.add(pane);

		// Liste Tranche Horaire
		JPanel thPanel = new JPanel();
		thPanel.setLayout(new BoxLayout(thPanel, BoxLayout.LINE_AXIS));
		liste2 = new JList();
		TrancheHoraireModel monModelTH = new TrancheHoraireModel();
		liste2.setModel(monModelTH);
		liste2.addListSelectionListener(this);
		liste2.setSize(getMaximumSize());
		liste2.setCellRenderer(renderer);
		JScrollPane pane2 = new JScrollPane(liste2);
		thPanel.add(labelTrancheHoraire);
		thPanel.add(pane2);

		// Duree
		JPanel dureePanel = new JPanel();
		dureePanel.setLayout(new BoxLayout(dureePanel, BoxLayout.LINE_AXIS));
		dureeTX = new JTextField();
		dureePanel.add(labelDuree);
		dureePanel.add(dureeTX);

		// Calendar
		JPanel calendarPannel = new JPanel();
		calendarPannel.setLayout(new BoxLayout(calendarPannel,
				BoxLayout.LINE_AXIS));
		calendar = new JCalendar();
		maDate = Calendar.getInstance();
		calendar.addPropertyChangeListener("calendar",
				new PropertyChangeListener() {

					@Override
					public void propertyChange(PropertyChangeEvent e) {
						final Calendar c = (Calendar) e.getNewValue();
						maDate = c;
					}
				});

		calendarPannel.add(calendar);

		// Client
		JPanel clientPannel = new JPanel();
		clientPannel
				.setLayout(new BoxLayout(clientPannel, BoxLayout.LINE_AXIS));
		clientTX = new JTextField();
		clientPannel.add(labelClient);
		clientPannel.add(clientTX);

		// Telephone
		JPanel telPannel = new JPanel();
		telPannel.setLayout(new BoxLayout(telPannel, BoxLayout.LINE_AXIS));
		telTX = new JTextField();
		telPannel.add(labelTel);
		telPannel.add(telTX);

		// Dispo
		JPanel dispoPannel = new JPanel();
		dispoPannel.setLayout(new BoxLayout(dispoPannel, BoxLayout.LINE_AXIS));
		dispoPannel.add(labelDispo);

		// JPanel droite
		JPanel panelDroite = new JPanel();
		panelDroite.setLayout(new BoxLayout(panelDroite, BoxLayout.Y_AXIS));
		panelDroite.add(typeSallePanel);
		panelDroite.add(thPanel);
		panelDroite.add(dureePanel);
		panelDroite.add(telPannel);
		panelDroite.add(clientPannel);
		panelDroite.setMinimumSize(new Dimension(250, 250));
		panelDroite.setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10));

		// Jpanel Gauche
		JPanel panelGauche = new JPanel();
		panelGauche.setLayout(new BoxLayout(panelGauche, BoxLayout.Y_AXIS));
		panelGauche.add(calendarPannel);
		panelGauche.add(boutonPanel);

		panelGauche.add(dispoPannel);
		panelGauche.setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10));

		JPanel panelGlobale = new JPanel();
		panelGlobale.setLayout(new BoxLayout(panelGlobale, BoxLayout.X_AXIS));
		panelGlobale.add(panelDroite);
		panelGlobale.add(panelGauche);
		add(panelGlobale);

	}

	@Override
	public void actionPerformed(ActionEvent e) {
		Object o = e.getSource();
		if (o == boutonPossible) {

			try {
				duree = Integer.parseInt(dureeTX.getText());
				int heureRetournee;
				heureRetournee = metier.isPossible(maDate, typeSalle,
						trancheHoraire, duree);
				labelDispo.setText("Le crenau dispo est � : " + heureRetournee
						+ "h");
				boutonReservation.setVisible(true);
			} catch (JourNonTravailler e1) {
				JOptionPane.showMessageDialog(dialog, e1.getMessage(),
						"Erreur", JOptionPane.ERROR_MESSAGE);
			} catch (TypeSalleInexistant e1) {
				JOptionPane.showMessageDialog(dialog, e1.getMessage(),
						"Erreur", JOptionPane.ERROR_MESSAGE);
			} catch (trancheHoraireInexistante e1) {
				JOptionPane.showMessageDialog(dialog, e1.getMessage(),
						"Erreur", JOptionPane.ERROR_MESSAGE);
			} catch (NumberFormatException e1) {
				JOptionPane.showMessageDialog(dialog, e1.getMessage(),
						"Erreur", JOptionPane.ERROR_MESSAGE);
			}

		} else if (o == boutonReservation) {
			numTel = Integer.parseInt(telTX.getText());
			nomClient = clientTX.getText();
			try {
				System.out.println((metier.effectuerReservation(maDate,
						typeSalle, trancheHoraire, duree, nomClient, numTel))
						.getIdReservation());
			} catch (TypeSalleInexistant e1) {
				JOptionPane.showMessageDialog(dialog, e1.getMessage(),
						"Erreur", JOptionPane.ERROR_MESSAGE);
			} catch (JourNonTravailler e1) {
				JOptionPane.showMessageDialog(dialog, e1.getMessage(),
						"Erreur", JOptionPane.ERROR_MESSAGE);
			} catch (trancheHoraireInexistante e1) {
				JOptionPane.showMessageDialog(dialog, e1.getMessage(),
						"Erreur", JOptionPane.ERROR_MESSAGE);
			} catch (NumberFormatException e1) {
				JOptionPane.showMessageDialog(dialog, e1.getMessage(),
						"Erreur", JOptionPane.ERROR_MESSAGE);
			}
		}
		return;

	}

	@Override
	public void valueChanged(ListSelectionEvent e) {
		if (e.getSource() == liste) {
			typeSalle = liste.getSelectedValue().toString();

		}
		if (e.getSource() == liste2) {
			trancheHoraire = liste2.getSelectedValue().toString();
		}

	}

}
