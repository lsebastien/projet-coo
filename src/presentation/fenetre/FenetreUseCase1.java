package presentation.fenetre;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.Calendar;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.ListCellRenderer;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import presentation.MonListCellRender;
import presentation.model.TypeSalleModel;

import com.toedter.calendar.JCalendar;

import exceptions.JourNonTravailler;
import exceptions.TypeSalleInexistant;
import metier.Metier;

public class FenetreUseCase1 extends JPanel implements ActionListener, ListSelectionListener {

	private JFrame frame;
	private JDialog dialog;
	private String typeSalle = "";
	private Calendar maDate;
	private JLabel labelTitre = new JLabel("Veuillez choisir votre Type de Salle");
	JCalendar calendar ;
	private JButton boutonAfficherResa = new JButton("Afiicher Planning");

	private JFrame frameUseCase1;
	private JList liste;

	private Metier metier;
	
	public FenetreUseCase1(JFrame Frame, Metier metier) {
		this.frame = frame;
		this.metier = metier;
		this.dialog = new JDialog();

		boutonAfficherResa.addActionListener(this);
		JPanel boutonPanel = new JPanel();
		boutonPanel.add(boutonAfficherResa);
		
		liste = new JList();
		TypeSalleModel monModel = new TypeSalleModel();
		liste.setModel(monModel);
		liste.addListSelectionListener(this);
		liste.setSize(getMaximumSize());
		ListCellRenderer renderer = new MonListCellRender();
		liste.setCellRenderer(renderer);
		JScrollPane pane = new JScrollPane (liste);
		maDate = Calendar.getInstance();
		calendar = new JCalendar();
		calendar.addPropertyChangeListener("calendar", new PropertyChangeListener() {

			@Override
			public void propertyChange(PropertyChangeEvent e) {
		        final Calendar c = (Calendar) e.getNewValue();   
		        maDate = c;
			}
		});
		
		labelTitre.setAlignmentX(Component.CENTER_ALIGNMENT);
		add(labelTitre);
		add(pane);
		add(calendar);
		add(boutonPanel);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		Object o = e.getSource();
		if (o == boutonAfficherResa) {
			try {
				afficherPlanning();
			} catch (JourNonTravailler e1) {
				JOptionPane.showMessageDialog(dialog,e1.getMessage(),"Erreur",JOptionPane.ERROR_MESSAGE);
			}
		}
		else
			return;

	}

	private void afficherPlanning() throws JourNonTravailler {
		
		try {
			JFrame fenetre = new JFrame();
			fenetre.getContentPane().add(new FenetreAfficherPlanning(fenetre,typeSalle,maDate));
			fenetre.setTitle("Visualisation Planning");
			fenetre.setExtendedState(JFrame.MAXIMIZED_BOTH);
			fenetre.setLocationRelativeTo(null);
			fenetre.setVisible(true);
			fenetre.pack();
		} catch (TypeSalleInexistant e) {
			JOptionPane.showMessageDialog(dialog,"Vous n'avez pas selectionner de type de salle","Erreur",JOptionPane.ERROR_MESSAGE);
		}
		
		
	}

	@Override
	public void valueChanged(ListSelectionEvent e) {
		if (e.getSource() == liste){
			typeSalle = liste.getSelectedValue().toString();
			
			
		}
		
	}

}
