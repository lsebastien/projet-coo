package presentation.fenetre;

import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.sql.SQLException;
import java.util.Calendar;
import java.util.List;

import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.ListCellRenderer;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import metier.UseCaseReservationAutomatique;
import metier.UseCaseReservationMulti;
import presentation.MonListCellRender;
import presentation.model.TrancheHoraireModel;
import presentation.model.TypeSalleModel;

import com.toedter.calendar.JCalendar;

import data.Reservation;
import exceptions.ClientInexistant;
import exceptions.JourNonTravailler;
import exceptions.TypeSalleInexistant;
import exceptions.trancheHoraireInexistante;

public class FenetreUseCaseResaMulti extends JPanel implements ActionListener, ListSelectionListener {

	private JFrame frame;
	private JDialog dialog;
	private JLabel labelTypeSalle = new JLabel("Votre Type de Salle :");
	private String typeSalle = "";
	private JLabel labelTrancheHoraire= new JLabel("Votre tranche horaire");
	private String trancheHoraire = "";
	private JLabel labelDuree = new JLabel("Votre duree");
	private int duree ;
	private JLabel labelSemaine = new JLabel("Nombre de semaine");
	private int nbsemaine ;
	private Calendar maDate;
	JCalendar calendar ;
	private JLabel labelClient = new JLabel("Votre nom");
	private String nomClient ="";
	private JLabel labelTel = new JLabel("Votre t�l�phone");
	private int numTel;
	private JButton boutonPossible = new JButton("R�server");
	private JLabel labelDispo = new JLabel("Les crenau dispo sont : ");

	private JFrame frameUseCase2;
	private JList liste;
	private JList liste2;

	private UseCaseReservationMulti metier;
	private JTextField dureeTX;
	private JTextField clientTX;
	private JTextField telTX;
	private JTextField semaineTX;
	
	public FenetreUseCaseResaMulti(JFrame Frame, UseCaseReservationMulti metierUseCaseMulti) {
		super(true);
		this.frame = frame;
		this.metier = metierUseCaseMulti;
		
		// Bouton
		boutonPossible.addActionListener(this);
		JPanel boutonPanel = new JPanel();
		boutonPanel.add(boutonPossible);
		
		//Liste Type Salle
		JPanel typeSallePanel = new JPanel();
		typeSallePanel.setLayout(new BoxLayout(typeSallePanel, BoxLayout.LINE_AXIS));
		liste = new JList();
		TypeSalleModel monModel = new TypeSalleModel();
		liste.setModel(monModel);
		liste.addListSelectionListener(this);
		liste.setSize(getMaximumSize());
		ListCellRenderer renderer = new MonListCellRender();
		liste.setCellRenderer(renderer);
		JScrollPane pane = new JScrollPane (liste);
		typeSallePanel.add(labelTypeSalle);
		typeSallePanel.add(pane);
		
		//Liste Tranche Horaire
		JPanel thPanel = new JPanel();
		thPanel.setLayout(new BoxLayout(thPanel, BoxLayout.LINE_AXIS));
		liste2 = new JList();
		TrancheHoraireModel monModelTH = new TrancheHoraireModel();
		liste2.setModel(monModelTH);
		liste2.addListSelectionListener(this);
		liste2.setSize(getMaximumSize());
		liste2.setCellRenderer(renderer);
		JScrollPane pane2 = new JScrollPane (liste2);
		thPanel.add(labelTrancheHoraire);
		thPanel.add(pane2);
		
		//Duree
		JPanel dureePanel = new JPanel();
		dureePanel.setLayout(new BoxLayout(dureePanel, BoxLayout.LINE_AXIS));
		dureeTX = new JTextField();
		dureePanel.add(labelDuree);
		dureePanel.add(dureeTX);
		
		//Semaine
		JPanel semainePanel = new JPanel();
		semainePanel.setLayout(new BoxLayout(semainePanel, BoxLayout.LINE_AXIS));
		semaineTX = new JTextField();
		semainePanel.add(labelSemaine);
		semainePanel.add(semaineTX);
		
		//Calendar
		JPanel calendarPannel = new JPanel();
		calendarPannel.setLayout(new BoxLayout(calendarPannel, BoxLayout.LINE_AXIS));
		calendar = new JCalendar();
		maDate = Calendar.getInstance();
		calendar.addPropertyChangeListener("calendar", new PropertyChangeListener() {
			
			@Override
			public void propertyChange(PropertyChangeEvent e) {
		        final Calendar c = (Calendar) e.getNewValue();   
		        maDate = c;
			}
		});
		
		calendarPannel.add(calendar);
		
		// Client
		JPanel clientPannel = new JPanel();
		clientPannel.setLayout(new BoxLayout(clientPannel, BoxLayout.LINE_AXIS));
		clientTX = new JTextField();
		clientPannel.add(labelClient);
		clientPannel.add(clientTX);
		
		//Telephone
		JPanel telPannel = new JPanel();
		telPannel.setLayout(new BoxLayout(telPannel, BoxLayout.LINE_AXIS));
		telTX = new JTextField();
		telPannel.add(labelTel);
		telPannel.add(telTX);
		
		//Dispo
		JPanel dispoPannel = new JPanel();
		dispoPannel.setLayout(new BoxLayout(dispoPannel, BoxLayout.LINE_AXIS));
		dispoPannel.add(labelDispo);
		
		//JPanel droite
		JPanel panelDroite = new JPanel();
		panelDroite.setLayout(new BoxLayout(panelDroite, BoxLayout.Y_AXIS));
		panelDroite.add(typeSallePanel);
		panelDroite.add(thPanel);
		panelDroite.add(dureePanel);
		panelDroite.add(semainePanel);
		panelDroite.add(telPannel);
		panelDroite.add(clientPannel);
		panelDroite.setMinimumSize(new Dimension(250, 250));
		panelDroite.setBorder(BorderFactory.createEmptyBorder(10,10,10,10));

		
		//Jpanel Gauche
		JPanel panelGauche = new JPanel();
		panelGauche.setLayout(new BoxLayout(panelGauche, BoxLayout.Y_AXIS));
		panelGauche.add(calendarPannel);
		panelGauche.add(boutonPanel);

		panelGauche.add(dispoPannel);
		panelGauche.setBorder(BorderFactory.createEmptyBorder(10,10,10,10));

		
		
		JPanel panelGlobale = new JPanel();
		panelGlobale.setLayout(new BoxLayout(panelGlobale, BoxLayout.X_AXIS));
		panelGlobale.add(panelDroite);
		panelGlobale.add(panelGauche);
		add(panelGlobale);

	}

	@Override
	public void actionPerformed(ActionEvent e) {
		Object o = e.getSource();
		if (o == boutonPossible) {
			
			try {
				duree = Integer.parseInt(dureeTX.getText());
				numTel = Integer.parseInt(telTX.getText());
				nomClient = clientTX.getText();
				nbsemaine = Integer.parseInt(semaineTX.getText());
				List<Reservation> heureRetournee = null;
				try {
					heureRetournee = metier.effectuerReservationMulti(maDate,typeSalle,trancheHoraire, duree,nomClient,numTel,nbsemaine);
				} catch (ClassNotFoundException | SQLException
						| ClientInexistant e1) {
					JOptionPane.showMessageDialog(dialog,e1.getMessage(),"Erreur",JOptionPane.ERROR_MESSAGE);
				}
				labelDispo.setText("Les crenau dispo sont :  ");
				for (Reservation r : heureRetournee){
					
					labelDispo.setText(labelDispo.getText() + "  le " + r.getDate().get(Calendar.DATE) + " � " + r.getHeureDebut()+ " h | ");
				}

			} catch (JourNonTravailler e1) {
				JOptionPane.showMessageDialog(dialog,e1.getMessage(),"Erreur",JOptionPane.ERROR_MESSAGE);
			} catch (TypeSalleInexistant e1) {
				JOptionPane.showMessageDialog(dialog,e1.getMessage(),"Erreur",JOptionPane.ERROR_MESSAGE);
			} catch (trancheHoraireInexistante e1) {
				JOptionPane.showMessageDialog(dialog,e1.getMessage(),"Erreur",JOptionPane.ERROR_MESSAGE);
			}
			catch (NumberFormatException e1){
				JOptionPane.showMessageDialog(dialog,e1.getMessage(),"Erreur",JOptionPane.ERROR_MESSAGE);
			}
			
		}


	}


	@Override
	public void valueChanged(ListSelectionEvent e) {
		if (e.getSource() == liste){
			typeSalle = liste.getSelectedValue().toString();
			
		}
		if (e.getSource() == liste2){
			trancheHoraire = liste2.getSelectedValue().toString();
		}
		
	}

}
