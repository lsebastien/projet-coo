package presentation.fenetre;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Dialog;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.sql.SQLException;
import java.util.Calendar;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.ListCellRenderer;
import javax.swing.border.Border;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import presentation.MonListCellRender;
import presentation.model.TrancheHoraireModel;
import presentation.model.TypeSalleModel;
import bdd.maConnection;

import com.toedter.calendar.JCalendar;

import data.Client;
import data.Forfait;
import data.Reservation;
import exceptions.ClientInconpatibleForfait;
import exceptions.ClientInexistant;
import exceptions.ForfaitExpirerException;
import exceptions.ForfaitIncompatibleException;
import exceptions.JourNonTravailler;
import exceptions.PasDeForfaitsException;
import exceptions.ReservationInexistante;
import exceptions.TypeForfaitInexistant;
import exceptions.TypeSalleInexistant;
import exceptions.trancheHoraireInexistante;
import fabrique.FabConnection;
import fabrique.FabForfait;
import fabrique.FabSQL;
import metier.Metier;
import metier.UseCaseEditerInfoClient;
import metier.UseCaseReservationAutomatique;

public class FenetreUseCase4Payement extends JPanel implements ActionListener {
	private JFrame frame;
	private float prix = 0;
	private UseCaseEditerInfoClient metier;
	private Dialog dialog;
	private Reservation resa;
	
	private JButton boutonPayer = new JButton("Payer");
	private JCheckBox boxforfait = new JCheckBox(" Utiliser un forfait");
	private JCheckBox boxfidelite = new JCheckBox("Utiliser les points");
	private JLabel labelnumForfait = new JLabel("N° de forfait");
	private JLabel prixReservation = new JLabel("Prix à payer : ");
	private JTextField champRechercheForfait = new JTextField(10);
	private Forfait utiliserForfait = null;

	private boolean utiliserPointsFidelite = false;

	public FenetreUseCase4Payement(JFrame Frame,
			UseCaseEditerInfoClient monUseCase2, Client c, Reservation r) {
		super(true);
		this.frame = Frame;
		this.metier = monUseCase2;
		this.resa = r;
		
		
		
		
		JPanel panelAction = new JPanel(new GridLayout(0, 1));
		JPanel panelForfait = new JPanel();
		JPanel panelFidelite = new JPanel();
		JPanel panelPrix = new JPanel();
		JPanel panelPayer = new JPanel();

		panelForfait.add(boxforfait);
		panelForfait.add(labelnumForfait);
		panelForfait.add(champRechercheForfait);

		champRechercheForfait.setVisible(false);
		labelnumForfait.setVisible(false);

		panelFidelite.add(boxfidelite);
		panelAction.add(panelForfait);
		panelPrix.add(prixReservation);
		panelPayer.add(boutonPayer);
		panelAction.add(panelFidelite);
		panelAction.add(panelPrix);
		panelAction.add(panelPayer);
		boutonPayer.addActionListener(this);

		setLayout(new BorderLayout());
		add(panelAction, BorderLayout.NORTH);

		if (c.getPfidelite() == 0) {
			panelFidelite.setVisible(false);
		}

		if (c.getMesForfaits().size() == 0) {
			panelForfait.setVisible(false);
		}

		champRechercheForfait.addFocusListener(new FocusAdapter() {
			public void focusLost(FocusEvent e) {
				String valeurNumForfait = champRechercheForfait.getText();
				try {

					if (!valeurNumForfait.equals("")) {

						utiliserForfait = FabForfait.getInstance()
								.rechercherForfait(
										Integer.parseInt(valeurNumForfait));
						if(prix == 0){
							JOptionPane.showMessageDialog(dialog, "Inutile d'utiliser les points le prix est déja de 0",
									"Erreur", JOptionPane.ERROR_MESSAGE);
							prixReservation.setText("Prix de la réservation :"+ prix) ;
						}else{
							 prix = metier.PayerReservation(
										resa,
										resa.getMonClient(),
										utiliserForfait, utiliserPointsFidelite);
							prixReservation.setText("Prix de la réservation :"+ prix) ;
						}
						

					} else {

						prix = metier.PayerReservation(
								resa,
								resa.getMonClient(),
								utiliserForfait, utiliserPointsFidelite);
					prixReservation.setText("Prix de la réservation :"+ prix) ;

					}
				} catch (ClassNotFoundException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				} catch (PasDeForfaitsException e1) {
					// TODO Auto-generated catch block
					JOptionPane.showMessageDialog(dialog, e1.getMessage(),
							"Erreur", JOptionPane.ERROR_MESSAGE);
				} catch (ForfaitIncompatibleException e1) {
					// TODO Auto-generated catch block
					JOptionPane.showMessageDialog(dialog, e1.getMessage(),
							"Erreur", JOptionPane.ERROR_MESSAGE);
				} catch (ForfaitExpirerException e1) {
					// TODO Auto-generated catch block
					JOptionPane.showMessageDialog(dialog, e1.getMessage(),
							"Erreur", JOptionPane.ERROR_MESSAGE);
				} catch (ClientInexistant e1) {
					// TODO Auto-generated catch block
					JOptionPane.showMessageDialog(dialog, e1.getMessage(),
							"Erreur", JOptionPane.ERROR_MESSAGE);
				} catch (SQLException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				} catch (ReservationInexistante e1) {
					// TODO Auto-generated catch block
					JOptionPane.showMessageDialog(dialog, e1.getMessage(),
							"Erreur", JOptionPane.ERROR_MESSAGE);
				} catch (TypeSalleInexistant e1) {
					// TODO Auto-generated catch block
					JOptionPane.showMessageDialog(dialog, e1.getMessage(),
							"Erreur", JOptionPane.ERROR_MESSAGE);
				} catch (ClientInconpatibleForfait e1) {
					// TODO Auto-generated catch block
					JOptionPane.showMessageDialog(dialog, e1.getMessage(),
							"Erreur", JOptionPane.ERROR_MESSAGE);
				} catch (NumberFormatException e1) {
					// TODO Auto-generated catch block
					JOptionPane.showMessageDialog(dialog, e1.getMessage(),
							"Erreur", JOptionPane.ERROR_MESSAGE);
				} catch (TypeForfaitInexistant e1) {
					// TODO Auto-generated catch block
					JOptionPane.showMessageDialog(dialog, e1.getMessage(),
							"Erreur", JOptionPane.ERROR_MESSAGE);
				}
			}
		});

		boxforfait.addItemListener(new ItemListener() {
			@Override
			public void itemStateChanged(ItemEvent e) {
				try {
					if (e.getStateChange() == ItemEvent.SELECTED) {
						System.out.println("Je select les forfaits");
						if(prix == 0){
							JOptionPane.showMessageDialog(dialog, "Inutile d'utiliser les forfaits le prix est déja de 0",
									"Erreur", JOptionPane.ERROR_MESSAGE);
							prixReservation.setText("Prix de la réservation :"+ prix) ;
							champRechercheForfait.setVisible(false);
							labelnumForfait.setVisible(false);
						}else{
							prix = metier.PayerReservation(
									resa,
									resa.getMonClient(),
									utiliserForfait, utiliserPointsFidelite);
						prixReservation.setText("Prix de la réservation :"+ prix) ;
							champRechercheForfait.setVisible(true);
							labelnumForfait.setVisible(true);
						}
						

						
					} else {
						champRechercheForfait.setVisible(false);
						champRechercheForfait.setText("");
						labelnumForfait.setVisible(false);
						System.out.println("Je deselect les forfaits");
						utiliserForfait = null;

						prix = metier.PayerReservation(
								resa,
								resa.getMonClient(),
								utiliserForfait, utiliserPointsFidelite);
					prixReservation.setText("Prix de la réservation :"+ prix) ;
							prixReservation.setText("Prix de la réservation :"+ prix) ;
					}
				} catch (ClassNotFoundException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				} catch (PasDeForfaitsException e1) {
					JOptionPane.showMessageDialog(dialog, e1.getMessage(),
							"Erreur", JOptionPane.ERROR_MESSAGE);
				} catch (ForfaitIncompatibleException e1) {
					JOptionPane.showMessageDialog(dialog, e1.getMessage(),
							"Erreur", JOptionPane.ERROR_MESSAGE);
				} catch (ForfaitExpirerException e1) {
					JOptionPane.showMessageDialog(dialog, e1.getMessage(),
							"Erreur", JOptionPane.ERROR_MESSAGE);
				} catch (ClientInexistant e1) {
					JOptionPane.showMessageDialog(dialog, e1.getMessage(),
							"Erreur", JOptionPane.ERROR_MESSAGE);
				} catch (SQLException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				} catch (ReservationInexistante e1) {
					JOptionPane.showMessageDialog(dialog, e1.getMessage(),
							"Erreur", JOptionPane.ERROR_MESSAGE);
				} catch (TypeSalleInexistant e1) {
					JOptionPane.showMessageDialog(dialog, e1.getMessage(),
							"Erreur", JOptionPane.ERROR_MESSAGE);
				} catch (ClientInconpatibleForfait e1) {
					JOptionPane.showMessageDialog(dialog, e1.getMessage(),
							"Erreur", JOptionPane.ERROR_MESSAGE);
				}

			}
		});
		
		boxfidelite.addItemListener(new ItemListener() {
			@Override
			public void itemStateChanged(ItemEvent e) {
				try {
					
					if (e.getStateChange() == ItemEvent.SELECTED) {
						
						
						utiliserPointsFidelite = true;
						System.out.println(utiliserForfait);

						
						if(prix == 0){
							JOptionPane.showMessageDialog(dialog, "Inutile d'utiliser les points le prix est déja de 0",
									"Erreur", JOptionPane.ERROR_MESSAGE);
							prixReservation.setText("Prix de la réservation :"+ prix) ;
						}else{
							prix = metier.PayerReservation(
									resa,
									resa.getMonClient(),
									utiliserForfait, utiliserPointsFidelite);
						prixReservation.setText("Prix de la réservation :"+ prix) ;
						}
								
						
					} else {
						utiliserPointsFidelite = false;
						
					
						prix = metier.PayerReservation(
								resa,
								resa.getMonClient(),
								utiliserForfait, utiliserPointsFidelite);
					prixReservation.setText("Prix de la réservation :"+ prix) ;
						}
				} catch (ClassNotFoundException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				} catch (PasDeForfaitsException e1) {
					JOptionPane.showMessageDialog(dialog, e1.getMessage(),
							"Erreur", JOptionPane.ERROR_MESSAGE);
				} catch (ForfaitIncompatibleException e1) {
					JOptionPane.showMessageDialog(dialog, e1.getMessage(),
							"Erreur", JOptionPane.ERROR_MESSAGE);
				} catch (ForfaitExpirerException e1) {
					JOptionPane.showMessageDialog(dialog, e1.getMessage(),
							"Erreur", JOptionPane.ERROR_MESSAGE);
				} catch (ClientInexistant e1) {
					JOptionPane.showMessageDialog(dialog, e1.getMessage(),
							"Erreur", JOptionPane.ERROR_MESSAGE);
				} catch (SQLException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				} catch (ReservationInexistante e1) {
					JOptionPane.showMessageDialog(dialog, e1.getMessage(),
							"Erreur", JOptionPane.ERROR_MESSAGE);
				} catch (TypeSalleInexistant e1) {
					JOptionPane.showMessageDialog(dialog, e1.getMessage(),
							"Erreur", JOptionPane.ERROR_MESSAGE);
				} catch (ClientInconpatibleForfait e1) {
					JOptionPane.showMessageDialog(dialog, e1.getMessage(),
							"Erreur", JOptionPane.ERROR_MESSAGE);
				}

			}
		});

		try {
			
			prix = metier.PayerReservation(
					resa,
					resa.getMonClient(),
					utiliserForfait, utiliserPointsFidelite);
		prixReservation.setText("Prix de la réservation :"+ prix) ;
		} catch (ClassNotFoundException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (PasDeForfaitsException e1) {
			JOptionPane.showMessageDialog(dialog, e1.getMessage(),
					"Erreur", JOptionPane.ERROR_MESSAGE);
		} catch (ForfaitIncompatibleException e1) {
			JOptionPane.showMessageDialog(dialog, e1.getMessage(),
					"Erreur", JOptionPane.ERROR_MESSAGE);
		} catch (ForfaitExpirerException e1) {
			JOptionPane.showMessageDialog(dialog, e1.getMessage(),
					"Erreur", JOptionPane.ERROR_MESSAGE);
		} catch (ClientInexistant e1) {
			JOptionPane.showMessageDialog(dialog, e1.getMessage(),
					"Erreur", JOptionPane.ERROR_MESSAGE);
		} catch (SQLException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (ReservationInexistante e1) {
			JOptionPane.showMessageDialog(dialog, e1.getMessage(),
					"Erreur", JOptionPane.ERROR_MESSAGE);
		} catch (TypeSalleInexistant e1) {
			JOptionPane.showMessageDialog(dialog, e1.getMessage(),
					"Erreur", JOptionPane.ERROR_MESSAGE);
		} catch (ClientInconpatibleForfait e1) {
			JOptionPane.showMessageDialog(dialog, e1.getMessage(),
					"Erreur", JOptionPane.ERROR_MESSAGE);
		}

	}

	@Override
	public void actionPerformed(ActionEvent e) {
		Object o = e.getSource();
		if(o == boutonPayer){
			
		int points;
		try {
			points = metier.AttribuerFidelité(resa);
			JOptionPane.showMessageDialog(dialog, "Vous avez gagner "+points+"points de fidelité vous en avez"+resa.getMonClient().getPfidelite(),
					"Erreur", JOptionPane.INFORMATION_MESSAGE);
			resa.setStatut("conf");
		} catch (ClassNotFoundException e2) {
			// TODO Auto-generated catch block
			e2.printStackTrace();
		} catch (ClientInexistant e2) {
			// TODO Auto-generated catch block
			e2.printStackTrace();
		} catch (SQLException e2) {
			// TODO Auto-generated catch block
			e2.printStackTrace();
		}
		
		try {
			
		
		
		FabSQL.getInstance().Update(resa);
		if(utiliserForfait != null){
		FabSQL.getInstance().Update(utiliserForfait);
		}
		
		} catch (ClassNotFoundException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (SQLException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		frame.dispose();
			
		}else{
			return;
		}
	}
}
