package presentation.fenetre;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dialog;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.SQLException;

import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.ListCellRenderer;
import javax.swing.ListSelectionModel;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import bdd.maConnection;
import presentation.model.ForfaitsModel;
import presentation.model.ListReservationRenderer;
import presentation.model.ReservationsModel;
import metier.UseCaseEditerInfoClient;
import metier.UseCaseReservationAutomatique;
import data.Client;
import data.Reservation;
import exceptions.ClientInexistant;
import fabrique.FabClient;
import fabrique.FabConnection;
import fabrique.FabSQL;



public class FenetreUseCase4 extends JPanel implements ActionListener, ListSelectionListener  {
	private Client c;
	private Dialog dialog;
	
	private JFrame frame;
	private JDialog dialogInfoPerso;
	private JDialog dialogSubordonnes;
	private JLabel titre = new JLabel("Edition des infos client");
	private JTextField champRechercheNom = new JTextField(10);
	private JTextField champRechercheTelephone = new JTextField(10);
	private JButton boutonRechercher = new JButton("OK");
	private JButton boutonPayerReservation = new JButton("Payer la reservation");
	private JButton boutonSupprimerReservation = new JButton("Supprimer la reservation");
	private JButton boutonAcheterForfait = new JButton("Acheter un forfait");
	private UseCaseEditerInfoClient metier;
	private JList ListeForfaits= new JList ();
	private JList<Reservation> ListeReservations= new JList ();
	public FenetreUseCase4(JFrame frame,UseCaseEditerInfoClient monUseCase4){
		
		this.frame = frame;
		this.metier = monUseCase4;

		JScrollPane listReservationsScrollPane = new JScrollPane (ListeReservations);        
		listReservationsScrollPane.setPreferredSize (new Dimension (200,200));
		listReservationsScrollPane.setMinimumSize (new Dimension (200,200));       
		listReservationsScrollPane.setBorder (BorderFactory.createTitledBorder(
				BorderFactory.createLineBorder (Color.cyan),
				"Liste des réservations"));
		ListeReservations.addListSelectionListener(this);
		
		JScrollPane listForfaitsScrollPane = new JScrollPane (ListeForfaits);        
		listForfaitsScrollPane.setPreferredSize (new Dimension (200,200));
		listForfaitsScrollPane.setMinimumSize (new Dimension (200,200));       
		listForfaitsScrollPane.setBorder (BorderFactory.createTitledBorder(
				BorderFactory.createLineBorder (Color.cyan),
				"Liste des forfaits"));
		ListeForfaits.addListSelectionListener(this);
		ListeReservations.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
	
	
		JPanel panelAction = new JPanel();
		JPanel panelReservations = new JPanel();
		JPanel panelForfaits = new JPanel();
		JPanel panelConfirmationVente = new JPanel();
		
		panelAction.add(champRechercheNom);
		panelAction.add(champRechercheTelephone);
		panelAction.add(boutonRechercher);
		boutonPayerReservation.setVisible(false);
		boutonAcheterForfait.setVisible(false);
		boutonSupprimerReservation.setVisible(false);
		panelConfirmationVente.add(boutonPayerReservation);
		panelConfirmationVente.add(boutonAcheterForfait);
		panelConfirmationVente.add(boutonSupprimerReservation);
		
		panelReservations.add(listReservationsScrollPane);
		panelReservations.setLayout(new BoxLayout(panelReservations,BoxLayout.X_AXIS));
		boutonRechercher.addActionListener(this);
		
		panelForfaits.add(listForfaitsScrollPane);
		panelForfaits.setLayout(new BoxLayout(panelForfaits,BoxLayout.X_AXIS));
		boutonRechercher.addActionListener(this);
		boutonPayerReservation.addActionListener(this);
		boutonSupprimerReservation.addActionListener(this);
		
		titre.setAlignmentX(Component.CENTER_ALIGNMENT);
		add(titre);
		
		
		
		setLayout (new BorderLayout());
		add(panelAction,BorderLayout.NORTH);
		add(panelConfirmationVente,BorderLayout.CENTER);
		add (panelForfaits, BorderLayout.EAST);
		add (panelReservations, BorderLayout.WEST);
		
	
	}
	
	@Override
	public void actionPerformed(ActionEvent e){
		Object o = e.getSource();
		
		if(o == boutonRechercher){
			String valeurNom = champRechercheNom.getText();
			String valeurTel = champRechercheTelephone.getText();
			try {
				c = FabClient.getInstance().rechercherClient(valeurNom, Integer.parseInt(valeurTel));
				ReservationsModel modelReservation = new ReservationsModel(c);
				ListeReservations.setModel(modelReservation);
				
				ListCellRenderer rendererReservations = new ListReservationRenderer();
				ListeReservations.setCellRenderer(rendererReservations);
				
				ForfaitsModel modelForfait = new ForfaitsModel(c);
				ListeForfaits.setModel(modelForfait);
				
				if(modelReservation.getSize() != 0){
					boutonPayerReservation.setVisible(true);
					boutonSupprimerReservation.setVisible(true);
				}else{
					boutonPayerReservation.setVisible(false);
					boutonSupprimerReservation.setVisible(false);
				}
			
				if(modelForfait.getSize() != 0){
					boutonAcheterForfait.setVisible(true);
				}else{
					boutonAcheterForfait.setVisible(false);
				}
				
			} catch (ClientInexistant e1){
				JOptionPane.showMessageDialog(dialog, e1.getMessage(),"Erreur",JOptionPane.ERROR_MESSAGE);
			} catch (NumberFormatException e1) {
				JOptionPane.showMessageDialog(dialog, "Le numéro de telephone doit etre composé exclusivement de chiffres","Erreur",JOptionPane.ERROR_MESSAGE);
			} catch (ClassNotFoundException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			} catch (SQLException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			
		}if(o == boutonPayerReservation){
			try{
			 int selected = ListeReservations.getSelectedIndex();
			 Reservation r = (Reservation)ListeReservations.getModel().getElementAt(selected);
			 if (r.getStatut().equals("conf")) {
					JOptionPane.showMessageDialog(dialog, "Cette réservation est déja payée","Erreur",JOptionPane.ERROR_MESSAGE);
	         }else{
	        	 UseCasePayerReservation(c,r);
	         }
			}
			catch(ArrayIndexOutOfBoundsException e1){
				JOptionPane.showMessageDialog(dialog,"Aucune réservation selectionnée","Erreur",JOptionPane.ERROR_MESSAGE);

			}
			
			
		
			
		}else if(o == boutonSupprimerReservation) {
			
			try {
				int selected = ListeReservations.getSelectedIndex();
				Reservation r = (Reservation)ListeReservations.getModel().getElementAt(selected);
				//ListeReservations.remove(selected);
				
				
				FabSQL.getInstance().Delete(r);
				ListeReservations.repaint();
				ListeReservations.revalidate();
			} catch (ClassNotFoundException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			} catch (SQLException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			catch(ArrayIndexOutOfBoundsException e1){
				JOptionPane.showMessageDialog(dialog,"Aucune réservation selectionnée","Erreur",JOptionPane.ERROR_MESSAGE);

			} catch (NumberFormatException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			} catch (ClientInexistant e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		}else{
		
			return;
		}
		
	}
	
	private void UseCasePayerReservation(Client c,Reservation r ) {
		UseCaseEditerInfoClient useCaseEditierInfo = new UseCaseEditerInfoClient();
		JFrame fenetre = new JFrame();
		fenetre.getContentPane().add(new FenetreUseCase4Payement(fenetre,useCaseEditierInfo,c,r),BorderLayout.CENTER);
		fenetre.setTitle("Payer la reservation");
		fenetre.setSize(800,400);
		fenetre.setLocationRelativeTo(null);
		fenetre.setVisible(true);
	}

	@Override
	public void valueChanged(ListSelectionEvent arg0) {
		// TODO Auto-generated method stub
		
	}
	
}
