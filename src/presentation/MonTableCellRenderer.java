package presentation;

import java.awt.Color;
import java.awt.Component;

import javax.swing.JLabel;
import javax.swing.JTable;
import javax.swing.ListCellRenderer;
import javax.swing.table.DefaultTableCellRenderer;

import data.Reservation;

public class MonTableCellRenderer extends DefaultTableCellRenderer {
	@Override
	public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
		super.getTableCellRendererComponent(table, value, isSelected, hasFocus,	row, column);
		
		String valeur = (String) value ;
		if (valeur.equals("libre")){
            setBackground(new Color(0, 205, 0) );
            setForeground(Color.white);
		}
		else if (valeur.equals("nc")){
			setBackground(Color.YELLOW);
			setForeground(Color.BLACK);
            setText("Reservation non confirm�e");
		}
		else if (valeur.equals("conf")){
			setBackground(Color.RED);
			setForeground(Color.white);
            setText("Reservation confirm�e");
		}
		else if (valeur.equals("hdc")){
			setBackground(Color.ORANGE);
			setForeground(Color.white);
		}
		else{
			setBackground(Color.BLACK);
			setForeground(Color.white);
		}
		table.setRowHeight(50);
		return this;
	}
}
