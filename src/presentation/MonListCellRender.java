package presentation;

import java.awt.Color;
import java.awt.Component;

import javax.swing.DefaultListCellRenderer;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.ListCellRenderer;
import javax.swing.border.Border;
import javax.swing.border.EmptyBorder;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

public class MonListCellRender extends JLabel implements ListCellRenderer {

	protected DefaultListCellRenderer defaultRenderer = new DefaultListCellRenderer();

	@Override
	public Component getListCellRendererComponent(JList liste, Object monObjet,
			int index, boolean isSelected, boolean cellHasFocus) {
		setText((String) monObjet);
		if (isSelected) {
            setBackground(new Color(0, 205, 0) );
            setForeground(Color.white);
        } else {
            setBackground(liste.getBackground());
            setForeground(liste.getForeground());
        }
		setFont(liste.getFont());
        setOpaque(true);
		return this;
	}

}
