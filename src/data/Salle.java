package data;

import java.util.ArrayList;
import java.util.List;

import fabrique.FabReservation;

public class Salle {
	
	
	private int idSalle;
	private String nomSalle;
	private TypeSalle monTypeSalle;
	private List<Reservation> mesReservations;
	
	public Salle(int idSalle, String nomSalle, TypeSalle monTypeSalle) {
		this.idSalle = idSalle;
		this.nomSalle = nomSalle;
		this.monTypeSalle = monTypeSalle;
		this.mesReservations = new ArrayList<Reservation>();
	}
	
	public int getIdSalle() {
		return idSalle;
	}
	public void setIdSalle(int idSalle) {
		this.idSalle = idSalle;
	}
	public String getNomSalle() {
		return nomSalle;
	}
	public void setNomSalle(String nomSalle) {
		this.nomSalle = nomSalle;
	}
	public TypeSalle getMonTypeSalle() {
		return monTypeSalle;
	}
	public void setMonTypeSalle(TypeSalle monTypeSalle) {
		this.monTypeSalle = monTypeSalle;
	}
	public List<Reservation> getMesReservations(){
		return  FabReservation.getInstance().getReservationByIdSalle(this.idSalle);
	}
	public void setMesReservations(List<Reservation> mesReservations) {
		this.mesReservations = mesReservations;
	}
	
	public void addUneReservation(Reservation r){
		mesReservations.add(r);
	}
	
	public void removeUneReservation(Reservation r){
		mesReservations.remove(r);
	}
	
	//TODO methodes ADD / REMOVE sur List mesReservations.

}
