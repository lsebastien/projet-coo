package data;

import java.util.Calendar;

public class Forfait {
	
	private int idForfait;
	private int heureRestantes;
	private Calendar dateExpiration;
	private Client monClient;
	private TypeForfait monTypeForfait;
	private TypeSalle monTypeSalle;
	
	
	public Forfait(int idForfait, int heureRestantes, Calendar dateExpiration,Client monClient, TypeForfait monTypeForfait, TypeSalle monTypeSalle) {
		
		this.idForfait = idForfait;
		this.heureRestantes = heureRestantes;
		this.dateExpiration = dateExpiration;
		this.monClient = monClient;
		this.monTypeForfait = monTypeForfait;
		this.monTypeSalle = monTypeSalle;
	}
	
	public int getId() {
		return idForfait;
	}
	public void setIdForfait(int idForfait) {
		this.idForfait = idForfait;
	}
	public int getHeureRestantes() {
		return heureRestantes;
	}
	public void setHeureRestantes(int heureRestantes) {
		this.heureRestantes = heureRestantes;
	}
	public Client getMonClient() {
		return monClient;
	}
	public void setMonClient(Client monClient) {
		this.monClient = monClient;
	}
	public Calendar getDateExpiration() {
		return dateExpiration;
	}
	public void setDateExpiration(Calendar dateExpiration) {
		this.dateExpiration = dateExpiration;
	}
	public TypeForfait getMonTypeForfait() {
		return monTypeForfait;
	}
	public void setMonTypeForfait(TypeForfait monTypeForfait) {
		this.monTypeForfait = monTypeForfait;
	}
	public TypeSalle getMonTypeSalle() {
		return monTypeSalle;
	}
	public void setMonTypeSalle(TypeSalle monTypeSalle) {
		this.monTypeSalle = monTypeSalle;
	}
	
}
