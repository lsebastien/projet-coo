package data;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import fabrique.FabClient;
import fabrique.FabForfait;
import fabrique.FabReservation;

public class Client {

	
	private int idClient;
	private String nomClient;
	private int telClient;
	private int pfidelite;
	private List<Forfait> mesForfaits;
	private List<Reservation> mesReservations;
	
	public Client(int idClient, String nomClient, int telClient) {
		
		this.idClient = idClient;
		this.nomClient = nomClient;
		this.telClient = telClient;
		this.pfidelite = 0;
		this.mesForfaits = new ArrayList<Forfait>();
		this.mesReservations = new ArrayList<Reservation>();
	}
	
	public int getId() {
		return idClient;
	}
	public void setIdClient(int idClient) {
		this.idClient = idClient;
	}
	public String getNomClient() {
		return nomClient;
	}
	public void setNomClient(String nomClient) throws ClassNotFoundException {
		try {
			this.nomClient = FabClient.getInstance().updateNomClient(this,nomClient);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	public int getTelClient() {
		return telClient;
	}
	public void setTelClient(int telClient) {
		this.telClient = telClient;
	}
	public int getPfidelite() {
		return pfidelite;
	}
	public void setPfidelite(int pfidelite) {
		this.pfidelite = pfidelite;
	}
	public List<Forfait> getMesForfaits() {
		return FabForfait.getInstance().rechercherForfaitByIdClient(this.idClient);
	}
	public void setMesForfaits(List<Forfait> mesForfaits) {
		this.mesForfaits = mesForfaits;
	}
	
	public List<Reservation> getMesReservations() {
		return FabReservation.getInstance().getReservationsByIdClient(this.idClient);
	}
	public void setMesReservations(List<Reservation> mesReservations) {
		this.mesReservations = mesReservations;
	}
	
	public void addUneReservation(Reservation r){
		mesReservations.add(r);
	}
	
	public void removeUneReservation(Reservation r){
		mesReservations.remove(r);
	}
	
	public void addUnForfait(Forfait f){
		mesForfaits.add(f);
	}
	
	public void removeUnForfait(Forfait f){
		mesForfaits.remove(f);
	}
	//TODO Ajouter ADD / REMOVE sur mesForfaits,mesReservation.
	
}
