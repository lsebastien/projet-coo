package data;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

public class TypeForfait {
	
	
	private int idTypeForfait;
	private int creditTemps;
	private int dureeValidite;
	private int prix;
	private List<Forfait> mesForfaits;
	
	public TypeForfait(int idTypeForfait, int creditTemps, int dureeValidite,
			int prix) {
		super();
		this.idTypeForfait = idTypeForfait;
		this.creditTemps = creditTemps;
		this.dureeValidite = dureeValidite;
		this.prix = prix;
		this.mesForfaits = new ArrayList<Forfait>();
	}
	
	public int getIdTypeForfait() {
		return idTypeForfait;
	}
	public void setIdTypeForfait(int idTypeForfait) {
		this.idTypeForfait = idTypeForfait;
	}
	public int getCreditTemps() {
		return creditTemps;
	}
	public void setCreditTemps(int creditTemps) {
		this.creditTemps = creditTemps;
	}
	public int getDureeValidite() {
		return dureeValidite;
	}
	public void setDureeValidite(int dureeValidite) {
		this.dureeValidite = dureeValidite;
	}
	public int getPrix() {
		return prix;
	}
	public void setPrix(int prix) {
		this.prix = prix;
	}
	public List<Forfait> getMesForfaits() {
		return mesForfaits;
	}
	public void setMesForfaits(List<Forfait> mesForfaits) {
		this.mesForfaits = mesForfaits;
	}
}
