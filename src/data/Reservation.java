package data;

import java.sql.SQLException;
import java.util.Calendar;

import exceptions.ClientInexistant;
import fabrique.FabClient;

public class Reservation {
	
	private int idReservation;
	private Calendar date;
	private int duree;
	private String horaire;
	private String statut;
	private Calendar dateReservation;
	private Calendar dateConfirmation;
	private int prix;
	private Client monClient;
	private Salle maSalle;
	private int heureDebut;
	
	public Reservation(int idReservation, Calendar date, int duree,	String horaire,int heureDebut, Calendar dateReservation,int prix,Client monClient, Salle maSalle) {
		
		this.idReservation = idReservation;
		this.date = date;
		this.duree = duree;
		this.heureDebut = heureDebut;
		this.horaire = horaire;
		this.statut = "nc";
		this.dateReservation = dateReservation;
		dateReservation.add(Calendar.DAY_OF_YEAR,10);
		this.dateConfirmation = dateReservation;
		this.prix = prix;
		this.monClient = monClient;
		this.maSalle = maSalle;
	}
	
	public int getId() {
		return idReservation;
	}
	public void setIdReservation(int idReservation) {
		this.idReservation = idReservation;
	}
	public Calendar getDate() {
		return date;
	}
	public void setDate(Calendar date) {
		this.date = date;
	}
	public int getDuree() {
		return duree;
	}
	public void setDuree(int duree) {
		this.duree = duree;
	}
	public String getHoraire() {
		return horaire;
	}
	public void setHoraire(String horaire) {
		this.horaire = horaire;
	}
	public String getStatut() {
		return statut;
	}
	public void setStatut(String statut) {
		this.statut = statut;
	}
	public Calendar getDateReservation() {
		return dateReservation;
	}
	public void setDateReservation(Calendar dateReservation) {
		this.dateReservation = dateReservation;
	}
	public Calendar getDateConfirmation() {
		return dateConfirmation;
	}
	public void setDateConfirmation(Calendar dateConfirmation) {
		this.dateConfirmation = dateConfirmation;
	}
	public int getPrix() {
		return prix;
	}
	public void setPrix(int prix) {
		this.prix = prix;
	}
	public Client getMonClient() throws ClassNotFoundException, ClientInexistant, SQLException {
		return FabClient.getInstance().rechercherClientByIdReservation(this.idReservation);
	}
	public void setMonClient(Client monClient) {
		this.monClient = monClient;
	}
	public Salle getMaSalle() {
		return maSalle;
	}
	public void setMaSalle(Salle maSalle) {
		this.maSalle = maSalle;
	}

	public int getHeureDebut() {
		return heureDebut;
	}

	public void setHeureDebut(int heureDebut) {
		this.heureDebut = heureDebut;
	}
}
