package data;

import java.util.ArrayList;
import java.util.List;

public class TypeSalle {
	
	public TypeSalle(int idType,String nomType) {
		super();
		this.idType = idType;
		this.nomType = nomType;
		this.mesForfaits = new ArrayList<Forfait>();
		this.mesSalles = new ArrayList<Salle>();
	}
	private int idType;
	private String nomType;
	
	private List<Salle> mesSalles;
	private List<Forfait> mesForfaits;
	
	public int getIdType() {
		return idType;
	}
	public void setIdType(int idType) {
		this.idType = idType;
	}
	
	public List<Salle> getMesSalles() {
		return mesSalles;
	}
	public void setMesSalles(List<Salle> mesSalles) {
		this.mesSalles = mesSalles;
	}
	public List<Forfait> getMesForfaits() {
		return mesForfaits;
	}
	public void setMesForfaits(List<Forfait> mesForfaits) {
		this.mesForfaits = mesForfaits;
	}
	public String getNomType() {
		return nomType;
	}
	public void setNomType(String nomType) {
		this.nomType = nomType;
	}
	
//TODO faire ADD / REMOVE pour la liste mesSalles,mesForfaits.
}
