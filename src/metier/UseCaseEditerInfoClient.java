package metier;

import java.sql.SQLException;
import java.util.Calendar;

import data.Client;
import data.Forfait;
import data.Reservation;
import exceptions.ClientInconpatibleForfait;
import exceptions.ClientInexistant;
import exceptions.ForfaitExpirerException;
import exceptions.ForfaitIncompatibleException;
import exceptions.PasDeForfaitsException;
import exceptions.ReservationInexistante;
import exceptions.TypeSalleInexistant;
import fabrique.FabClient;
import fabrique.FabReservation;
import fabrique.FabSQL;

public class UseCaseEditerInfoClient {
	
	public float PayerReservation(Reservation idReservation, Client Client,
			Forfait Forfait, boolean utiliserPointFidelite)
			throws PasDeForfaitsException, ForfaitIncompatibleException,
			ForfaitExpirerException, ClassNotFoundException, ClientInexistant, SQLException, ReservationInexistante, TypeSalleInexistant, ClientInconpatibleForfait {
		float prix = 0;
		Reservation laReservation = idReservation;
		Client c = Client;
		System.out.println("Avant "+c.getPfidelite());
		int nb_heure_gratuite_utilisees = 0;
		
		
		if (utiliserPointFidelite == true && c.getPfidelite() >= 150){
			int points_fidelite = c.getPfidelite();
			int nb_heure_gratuite_dispo = (points_fidelite / 10) * 2;

			
				
			if (nb_heure_gratuite_dispo > laReservation.getDuree()) {
				nb_heure_gratuite_utilisees = laReservation.getDuree();
				nb_heure_gratuite_dispo -= nb_heure_gratuite_utilisees;
				c.setPfidelite(nb_heure_gratuite_dispo * 10);
				
			} else {
				nb_heure_gratuite_utilisees = nb_heure_gratuite_dispo;
				nb_heure_gratuite_dispo -= nb_heure_gratuite_utilisees;
			

			}

			
		}

		if (Forfait != null){
			
			if (Forfait.getMonClient().getId() !=	laReservation.getMonClient().getId()) {
				throw new ClientInconpatibleForfait("Le client : "+ c.getNomClient()+ " n'est pas propriétaire de ce forfait ! ");
						

			}
			
			if (!Forfait.getMonTypeSalle().getNomType().equals(
					laReservation.getMaSalle().getMonTypeSalle().getNomType())) {
				throw new ForfaitIncompatibleException("Le client : "
						+ c.getNomClient()
						+ " ne dispose pas de forfaits pour les salles de type"
						+ laReservation.getMaSalle().getMonTypeSalle().getNomType());

			}
			if (Forfait.getDateExpiration().compareTo(Calendar.getInstance()) < 0) {
				System.out.println("Expiration : "+Forfait.getDateExpiration().getTime());
				System.out.println("Maintenant : "+Calendar.getInstance().getTime());
				throw new ForfaitExpirerException("Le forfait "
						+ Forfait.getId() + " est expirÃ©");
			}
			
			if (Forfait.getHeureRestantes() == 0) {
				throw new ForfaitExpirerException("Le forfait "
						+ Forfait.getId() + " est vide");
			}
			
			if (Forfait.getHeureRestantes() < laReservation.getDuree()) {
				System.out.println(Forfait.getHeureRestantes() + " - Heures");
				if (laReservation.getHoraire() == "Soir") {
					prix = (float) (Metier.PrixReservation(
							laReservation.getMaSalle(),
							laReservation.getDuree()
									- Forfait.getHeureRestantes()
									- nb_heure_gratuite_utilisees) * 1.02);
				} else {
					prix = Metier.PrixReservation(laReservation.getMaSalle(),laReservation.getDuree()- Forfait.getHeureRestantes()									- nb_heure_gratuite_utilisees);
				}
				Forfait.setHeureRestantes(0);

			}else{
				Forfait.setHeureRestantes(Forfait.getHeureRestantes() - laReservation.getDuree() - nb_heure_gratuite_utilisees);
			}
		} else {
			if (laReservation.getHoraire() == "Soir") {
				prix = (float) (Metier.PrixReservation(
						laReservation.getMaSalle(), laReservation.getDuree()
								- nb_heure_gratuite_utilisees) * 1.02);
			} else {
				prix = Metier.PrixReservation(laReservation.getMaSalle(),
						laReservation.getDuree() - nb_heure_gratuite_utilisees);
			}
		}

		
		laReservation.setStatut("conf");
		System.out.println("Apres "+c.getPfidelite());
		return prix;

	}
	
	public int AttribuerFidelité(Reservation r) throws ClassNotFoundException, ClientInexistant, SQLException{
		Client c = r.getMonClient();
		c .setPfidelite(r.getMonClient().getPfidelite() + r.getDuree() * 5);
		System.out.println(c .getPfidelite());
		FabSQL.getInstance().Update(c);
		return  r.getDuree() * 5;
	}
	
	public int retirerCréditforfait(Reservation laReservation, Forfait Forfait){
		int nb_heure_gratuite_utilisees = 0;
		if (Forfait.getHeureRestantes() < laReservation.getDuree()) {
			
			Forfait.setHeureRestantes(0);

		}else{
			Forfait.setHeureRestantes(Forfait.getHeureRestantes() - laReservation.getDuree() - nb_heure_gratuite_utilisees);
		}
		return Forfait.getHeureRestantes();
	}

}
