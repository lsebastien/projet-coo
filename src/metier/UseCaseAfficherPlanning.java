package metier;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import data.Reservation;
import data.Salle;
import data.TypeSalle;
import exceptions.JourNonTravailler;
import exceptions.TypeSalleInexistant;
import fabrique.FabReservation;
import fabrique.FabSalle;
import fabrique.FabTypeSalle;

public class UseCaseAfficherPlanning {
	
	public Map<String, List<Reservation>> getInfoPlanning (String typeSalle , Calendar maDate) throws TypeSalleInexistant, JourNonTravailler{
		
		UseCaseReservationAutomatique us = new UseCaseReservationAutomatique();
		Map<String, List<Reservation>> map = new HashMap<String, List<Reservation>>();
		List<Salle> mesSalles = new ArrayList<Salle>();
		TypeSalle ts = FabTypeSalle.getInstance().rechercherTypeSalle(typeSalle);
		mesSalles = FabSalle.getInstance().rechercherSalleByTypeSalle(ts.getIdType());
	
		if(us.isJourTravaille(maDate)){
			for (Salle s : mesSalles){
				
				List<Reservation> lesReservations = new ArrayList<Reservation>();
				for (Reservation r : s.getMesReservations()){
				
					if (isSameDay(r.getDate(), maDate)){
						lesReservations.add(r);
					}
				}
				map.put(s.getNomSalle(), lesReservations);
			}
		
			return map;
		}
		else throw new JourNonTravailler("Ce jour n'est pas travaillé désolé");
		
	}

	public boolean isSameDay (Calendar c1 , Calendar c2){
		if (c1.get(Calendar.YEAR) == c2.get(Calendar.YEAR) && c1.get(Calendar.DAY_OF_YEAR) == c2.get(Calendar.DAY_OF_YEAR)){
			return true;
		}
		else 
			return false;
	}
}
