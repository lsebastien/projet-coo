package metier;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;

import data.Client;
import data.Reservation;
import data.Salle;
import data.TypeSalle;
import exceptions.ClientInexistant;
import exceptions.JourNonTravailler;
import exceptions.TypeSalleInexistant;
import exceptions.trancheHoraireInexistante;
import fabrique.FabClient;
import fabrique.FabReservation;
import fabrique.FabSalle;
import fabrique.FabTypeSalle;

public class UseCaseReservationAutomatique {

	public Reservation effectuerReservation (Calendar laDate, String nomType,String horaire,int duree,String nomClient, int tel) throws TypeSalleInexistant, JourNonTravailler, trancheHoraireInexistante{
		Client c;
		try {
			c = FabClient.getInstance().rechercherClient(nomClient, tel);
			Salle s = trouverSalleLibre(nomType, laDate, horaire, duree);
			System.out.println(s.getNomSalle());
			int heureDebut = isPossible(laDate, nomType, horaire, duree);
			if(heureDebut != 0){
				Reservation r1 = FabReservation.getInstance().creerReservation(laDate,duree,horaire,heureDebut,Calendar.getInstance(),metier.Metier.PrixReservation(s,duree), c, s);
				return r1;
			}	
		} catch (ClassNotFoundException | ClientInexistant | SQLException e) {
			e.printStackTrace();
		}
		return null;
	}
	
	public Salle trouverSalleLibre(String typeSalle,Calendar laDate,String horaire,int duree) throws TypeSalleInexistant, trancheHoraireInexistante{
		TypeSalle ts = FabTypeSalle.getInstance().rechercherTypeSalle(typeSalle);
		List<Salle> mesSalles = new ArrayList<Salle>();
		mesSalles = FabSalle.getInstance().rechercherSalleByTypeSalle(ts.getIdType());
		for (Salle s : mesSalles){
			int duree_occupe_salle = 0 ;

			for (Reservation r : s.getMesReservations()){
				if (r.getHoraire().equals(horaire) && r.getDate().equals(laDate)){
					duree_occupe_salle += r.getDuree();
				}
			}

			int duree_disponible = dureeTrancheHoraire(horaire);

			if ((duree_occupe_salle + duree) <= duree_disponible){
				return s;
			}
		}
		return null;
	}
	
	//Retourne l'heure de debut de la reservation si possible
	//sinon retourne 0
	public int isPossible (Calendar laDate, String nomType,String horaire,int duree) throws JourNonTravailler, TypeSalleInexistant, trancheHoraireInexistante{
		
		List<Salle> mesSalles = new ArrayList<Salle>();
		if (isJourTravaille(laDate)){
			
			TypeSalle ts = FabTypeSalle.getInstance().rechercherTypeSalle(nomType);
			mesSalles = FabSalle.getInstance().rechercherSalleByTypeSalle(ts.getIdType());
			for (Salle s : mesSalles){
				int duree_occupe_salle = 0 ;

				for (Reservation r : s.getMesReservations()){
					if (r.getHoraire().equals(horaire) && r.getDate().equals(laDate)){
						duree_occupe_salle += r.getDuree();
					}
				}

				int duree_disponible = dureeTrancheHoraire(horaire);

				if ((duree_occupe_salle + duree) <= duree_disponible){
					System.out.println(heureDebutTrancheHoraire(horaire)+"    "+duree_occupe_salle);
					return (heureDebutTrancheHoraire(horaire) + duree_occupe_salle) ;
					
				}
				
			}
		}
		else {
			throw new JourNonTravailler("Ce jour n'est pas travailler");
		}

		return 0;
	}
	
	private int heureDebutDisponible(Salle s,int duree,String horaire){
		int heure_debut = heureDebutTrancheHoraire(horaire);
		List<Reservation> lesReservations = s.getMesReservations();
		
		Collections.sort(lesReservations, new Comparator<Reservation>(){
	        @Override public int compare(Reservation r1, Reservation r2){
	            return r1.getHeureDebut() - r2.getHeureDebut();
	        }
		});
		
		for(Reservation r : lesReservations){
			if(r.getHoraire().equals(horaire)){
				if(heure_debut == r.getHeureDebut()){
					heure_debut += r.getDuree();
				}
			}
		}
		
		
		return 0;
		
	}
	
	public List<Reservation> RechercheReservationTypeSalle(String typeSalle) {
		List<Reservation> mesReservations = new ArrayList<Reservation>();
		for(Iterator<Reservation> iter = FabReservation.getInstance().listerReservation(); iter.hasNext();) {
			Reservation r  = iter.next();
			if (r.getMaSalle().getMonTypeSalle().getNomType().equals(typeSalle)) {
				mesReservations.add(r);
			}
		}
		return mesReservations;
	}
	
	
	
	private int dureeDisponibilite(int heureDebut,String typeSalle){
		return 0;
	}
	
	//Retourne l'heure de debut selon la tranche horaire
	private int heureDebutTrancheHoraire(String horaire) {
		int heure_debut = 0;
		if(horaire.equals("Matin")){
			heure_debut = 9;
		}else if(horaire.equals("Midi")){
			heure_debut = 13;
		}else if(horaire.equals("Soir")){
			heure_debut = 20;
		}
		return heure_debut;
	}
	
	//Retourne la duree d'une tranche horaire
	private int dureeTrancheHoraire(String horaire) throws trancheHoraireInexistante {
		int duree_disponible = 0;
		if(horaire.equals("Matin")){
			duree_disponible = 4;
		}else if(horaire.equals("Midi")){
			duree_disponible = 7;
		}else if(horaire.equals("Soir")){
			duree_disponible = 4;
		}
		else {
			throw new trancheHoraireInexistante("Cette tranche horaire n'existe pas");
		}

		return duree_disponible;

	}
	
	//Retourne la liste de toutes les salles selon leur cat�gorie
	List<Salle> rechercherSalleType(String nomType) {
		List<Salle> mesSalles = new ArrayList<Salle>();
		for(Iterator<Salle> iter = FabSalle.getInstance().listerSalle(); iter.hasNext();) {
			Salle s  = iter.next();
			if (s.getMonTypeSalle().getNomType().equals(nomType)){
				mesSalles.add(s);
			}
		}
		return mesSalles;
	}
	
	//Retourne true si le jour est travaill�
	//Sinon false
	public boolean isJourTravaille(Calendar p_date)
	{
		// On constitue la liste des jours fériés
		final List < Calendar > joursFeries = new ArrayList < Calendar > ();
		// On recherche les jours fériés de l'année de la date en paramètre
		final Calendar jourFerie = (Calendar) p_date.clone();
		jourFerie.set(jourFerie.get(Calendar.YEAR), Calendar.JANUARY, 1);
		joursFeries.add((Calendar) jourFerie.clone());
		jourFerie.set(jourFerie.get(Calendar.YEAR), Calendar.MAY, 1);
		joursFeries.add((Calendar) jourFerie.clone());
		jourFerie.set(jourFerie.get(Calendar.YEAR), Calendar.MAY, 8);
		joursFeries.add((Calendar) jourFerie.clone());
		jourFerie.set(jourFerie.get(Calendar.YEAR), Calendar.JULY, 14);
		joursFeries.add((Calendar) jourFerie.clone());
		jourFerie.set(jourFerie.get(Calendar.YEAR), Calendar.AUGUST, 15);
		joursFeries.add((Calendar) jourFerie.clone());
		jourFerie.set(jourFerie.get(Calendar.YEAR), Calendar.NOVEMBER, 1);
		joursFeries.add((Calendar) jourFerie.clone());
		jourFerie.set(jourFerie.get(Calendar.YEAR), Calendar.NOVEMBER, 11);
		joursFeries.add((Calendar) jourFerie.clone());
		jourFerie.set(jourFerie.get(Calendar.YEAR), Calendar.DECEMBER, 25);
		joursFeries.add((Calendar) jourFerie.clone());

		// Calcul du jour de pâques (algorithme de Oudin (1940))
		// Calcul du nombre d'or - 1
		final int intGoldNumber = p_date.get(Calendar.YEAR) % 19;
		// Année divisé par cent
		final int intAnneeDiv100 = (int) (p_date.get(Calendar.YEAR) / 100);
		// intEpacte est = 23 - Epacte (modulo 30)
		final int intEpacte = (intAnneeDiv100 - intAnneeDiv100 / 4
				- (8 * intAnneeDiv100 + 13) / 25
				+ (19 * intGoldNumber) + 15) % 30;
		// Le nombre de jours à partir du 21 mars
		// pour atteindre la pleine lune Pascale
		final int intDaysEquinoxeToMoonFull = intEpacte - (intEpacte / 28)
				* (1 - (intEpacte / 28) * (29 / (intEpacte + 1))
						* ((21 - intGoldNumber) / 11));
		// Jour de la semaine pour la pleine lune Pascale (0=dimanche)
		final int intWeekDayMoonFull = (p_date.get(Calendar.YEAR)
				+ p_date.get(Calendar.YEAR) / 4
				+ intDaysEquinoxeToMoonFull + 2 - intAnneeDiv100
				+ intAnneeDiv100 / 4) % 7;
		// Nombre de jours du 21 mars jusqu'au dimanche de ou
		// avant la pleine lune Pascale (un nombre entre -6 et 28)
		final int intDaysEquinoxeBeforeFullMoon =
				intDaysEquinoxeToMoonFull - intWeekDayMoonFull;
		// mois de pâques
		final int intMonthPaques = 3
				+ (intDaysEquinoxeBeforeFullMoon + 40) / 44;
		// jour de pâques
		final int intDayPaques = intDaysEquinoxeBeforeFullMoon + 28
				- 31 * (intMonthPaques / 4);
		// lundi de pâques
		jourFerie.set(
				p_date.get(Calendar.YEAR), intMonthPaques - 1, intDayPaques + 1);
		final Calendar lundiDePaque = (Calendar) jourFerie.clone();
		joursFeries.add(lundiDePaque);
		// Ascension
		final Calendar ascension = (Calendar) lundiDePaque.clone();
		ascension.add(Calendar.DATE, 38);
		joursFeries.add(ascension);
		//Pentecote
		final Calendar lundiPentecote = (Calendar) lundiDePaque.clone();
		lundiPentecote.add(Calendar.DATE, 48);
		joursFeries.add(lundiPentecote);
		if (joursFeries.contains(p_date)
				|| p_date.get(Calendar.DAY_OF_WEEK) == Calendar.MONDAY) {
			return false;
		}
		return true;
	}
	
	public boolean supprimerReservation(Reservation laReservation) throws ClassNotFoundException, ClientInexistant, SQLException{
		if(laReservation.getStatut().equals("nc") || laReservation.getStatut().equals("rhd") || laReservation.getStatut().equals("nc") && laReservation.getDate().compareTo(Calendar.getInstance()) < 0){
			Client leClient = laReservation.getMonClient();
			Salle laSalle = laReservation.getMaSalle();
			leClient.removeUneReservation(laReservation);
			laSalle.removeUneReservation(laReservation);
			laReservation = null;
			return true;
		}
		return false;
	}
		
	

}
