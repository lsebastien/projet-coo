package metier;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import data.Client;
import data.Reservation;
import data.Salle;
import exceptions.ClientInexistant;
import exceptions.JourNonTravailler;
import exceptions.TypeSalleInexistant;
import exceptions.trancheHoraireInexistante;
import fabrique.FabClient;
import fabrique.FabReservation;

public class UseCaseReservationMulti {
	
	public List<Reservation> effectuerReservationMulti (Calendar laDate, String nomType,String horaire,int duree,String nomClient, int tel,int nbsemaine) throws JourNonTravailler, TypeSalleInexistant, trancheHoraireInexistante, ClassNotFoundException, SQLException, ClientInexistant{
		
		UseCaseReservationAutomatique us = new UseCaseReservationAutomatique();
		Client c = FabClient.getInstance().rechercherClient(nomClient, tel);
		Salle s = us.trouverSalleLibre(nomType, laDate, horaire, duree);
		List<Reservation> heuresDebuts = new ArrayList();
		Calendar dateDepart = (Calendar) laDate.clone() ;
		// On verifie que la reservation est possible pour toutes les semaines 
		for (int i=0;i<nbsemaine;i++){
			
			if(us.isPossible(laDate, nomType, horaire, duree)!=0){
				laDate.add(Calendar.WEEK_OF_YEAR,1);
			}
			else{
				return heuresDebuts;
			}
		}
		
		for (int i=0;i<nbsemaine;i++){

			Calendar date = Calendar.getInstance();
			date = (Calendar) dateDepart.clone();
			int heureDebut = us.isPossible(dateDepart, nomType, horaire, duree);
			Reservation r1 = FabReservation.getInstance().creerReservation(date,duree,horaire,heureDebut,Calendar.getInstance(),metier.Metier.PrixReservation(s,duree), c, s);
			heuresDebuts.add(r1);
			dateDepart.add(Calendar.WEEK_OF_YEAR,1);
			}
		
		return heuresDebuts;
		
	}

}
