package metier;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import data.Client;
import data.Forfait;
import data.Reservation;
import data.Salle;
import data.TypeSalle;
import exceptions.ClientInexistant;
import exceptions.ForfaitExpirerException;
import exceptions.ForfaitIncompatibleException;
import exceptions.ForfaitTempsInsuffisantException;
import exceptions.PasDeForfaitsException;
import exceptions.ReservationInexistante;
import exceptions.TypeSalleInexistant;
import fabrique.FabClient;
import fabrique.FabForfait;
import fabrique.FabReservation;
import fabrique.FabTypeSalle;

public class Metier {

	public List<Reservation> RechercheReservationTypeSalle(String typeSalle) throws ClassNotFoundException, SQLException, ClientInexistant, TypeSalleInexistant {
		List<Reservation> mesReservations = new ArrayList<Reservation>();
		for (Iterator<Reservation> iter = FabReservation.getInstance()
				.listerReservation(); iter.hasNext();) {
			Reservation r = iter.next();
			if (r.getMaSalle().getMonTypeSalle().getNomType().equals(typeSalle)) {
				mesReservations.add(r);
			}
		}
		return mesReservations;
	}

	

	public static int PrixReservation(Salle salle, int durree) {

		int prix = 0;
		int nbPlagesDeuxHeures = durree / 2;
		int nbPlagesUneHeure = durree % 2;

		if (salle.getMonTypeSalle().getNomType().equals("Petite")) {
			prix += nbPlagesDeuxHeures * 10 + nbPlagesUneHeure * 7;
		} else if (salle.getMonTypeSalle().getNomType().equals("Moyenne")) {
			prix += nbPlagesDeuxHeures * 16 + nbPlagesUneHeure * 10;
		} else {
			prix += nbPlagesDeuxHeures * 30 + nbPlagesUneHeure * 20;
		}
		return prix;
	}

	public float PayerReservation(int idReservation, int idClient,
			Forfait leForfait, boolean utiliserPointFidelite)
			throws PasDeForfaitsException, ForfaitIncompatibleException,
			ForfaitExpirerException, ClassNotFoundException, ClientInexistant, SQLException, ReservationInexistante, TypeSalleInexistant {
		float prix = 0;
		Reservation laReservation = FabReservation.getInstance()
				.rechercherReservation(idReservation);
		Client c = FabClient.getInstance().rechercherClientById(idClient);
		int nb_heure_gratuite_utilisees = 0;
		
		
		if (utiliserPointFidelite == true && c.getPfidelite() >= 10){
			int points_fidelite = c.getPfidelite();
			int nb_heure_gratuite_dispo = (points_fidelite / 10) * 2;

			System.out
					.println("Nombre d'heures grauites dispo avec points de fidelite : "
							+ nb_heure_gratuite_dispo);
			if (nb_heure_gratuite_dispo > laReservation.getDuree()) {
				nb_heure_gratuite_utilisees = laReservation.getDuree();
				nb_heure_gratuite_dispo -= nb_heure_gratuite_utilisees;
				c.setPfidelite(nb_heure_gratuite_dispo * 10);
				System.out.println("Nombre de points de fidelite 1: "
						+ c.getPfidelite());
			} else {
				nb_heure_gratuite_utilisees = nb_heure_gratuite_dispo;
				nb_heure_gratuite_dispo -= nb_heure_gratuite_utilisees;
				System.out.println("Nombre de points de fidelite 2: "
						+ c.getPfidelite());
				c.setPfidelite(0);

			}

			System.out
					.println("Nombre d'heures grauites utilisée avec points de fidelite : "
							+ nb_heure_gratuite_utilisees);
			System.out
					.println("Nombre d'heures grauites restantes avec points de fidelite : "
							+ nb_heure_gratuite_dispo);

		}

		if (leForfait != null){
			System.out.println("JE T ENCULE");
			if (!leForfait.getMonTypeSalle().equals(
					laReservation.getMaSalle().getMonTypeSalle())) {
				throw new ForfaitIncompatibleException("Le client : "
						+ c.getNomClient()
						+ " ne dispose pas de forfaits pour les salles de type"
						+ laReservation.getMaSalle().getMonTypeSalle());

			}
			if (leForfait.getDateExpiration().compareTo(Calendar.getInstance()) < 0) {
				throw new ForfaitExpirerException("Le forfait "
						+ leForfait.getId() + " est expirÃ©");
			}
			
			if (leForfait.getHeureRestantes() == 0) {
				throw new ForfaitExpirerException("Le forfait "
						+ leForfait.getId() + " est vide");
			}
			
			if (leForfait.getHeureRestantes() < laReservation.getDuree()) {
				System.out.println(leForfait.getHeureRestantes() + " - Heures");
				if (laReservation.getHoraire() == "Soir") {
					prix = (float) (Metier.PrixReservation(
							laReservation.getMaSalle(),
							laReservation.getDuree()
									- leForfait.getHeureRestantes()
									- nb_heure_gratuite_utilisees) * 1.02);
				} else {
					prix = Metier.PrixReservation(laReservation.getMaSalle(),laReservation.getDuree()- leForfait.getHeureRestantes()									- nb_heure_gratuite_utilisees);
				}
				leForfait.setHeureRestantes(0);

			}else{
				leForfait.setHeureRestantes(leForfait.getHeureRestantes() - laReservation.getDuree() - nb_heure_gratuite_utilisees);
			}
		} else {
			if (laReservation.getHoraire() == "Soir") {
				prix = (float) (Metier.PrixReservation(
						laReservation.getMaSalle(), laReservation.getDuree()
								- nb_heure_gratuite_utilisees) * 1.02);
			} else {
				prix = Metier.PrixReservation(laReservation.getMaSalle(),
						laReservation.getDuree() - nb_heure_gratuite_utilisees);
			}
		}

		c.setPfidelite(c.getPfidelite() + laReservation.getDuree() * 5);
		laReservation.setStatut("conf");
		return prix;

	}

}
